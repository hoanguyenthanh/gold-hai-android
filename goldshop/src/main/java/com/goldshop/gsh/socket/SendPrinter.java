package com.goldshop.gsh.socket;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by HOA on 12/06/2017.
 */

public class SendPrinter {
    private String message = "Hi client!";
    private static String kq = "";
    private ClientTask myClientTask;
    private OnListenerStatementPrint mOnListenerStatementPrint;
    private static boolean flag = true;

    private Socket mSocketPrinter = null;
    private static String mAddress;
    private static int mPort;

    public interface OnListenerStatementPrint {
        void sentStatementPrint(String text);
    }

    public void setOnListenerStatementPrint(OnListenerStatementPrint listener) {
        mOnListenerStatementPrint = listener;
    }

    public SendPrinter (){
        mAddress = "192.168.1.96";
        mPort = 100;
    }

    public void sendStatement(long orderId){
        myClientTask = new ClientTask();
        myClientTask.execute();

        setOnListenerStatementPrint(myClientTask);
        mOnListenerStatementPrint.sentStatementPrint(String.valueOf(orderId));
    }

    private static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (flag) {
                kq += msg.obj.toString() + "\r\n";
                Log.d("handleMessage", kq);
            }
        }
    };

    private class ClientTask extends AsyncTask<String, String, String>
            implements OnListenerStatementPrint {
        private PrintWriter mPrintWriter;

        @Override
        public void sentStatementPrint(String text) {
            sendMessage(text);
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String strOrderId = params[0].toString();
                mSocketPrinter = new Socket(mAddress, mPort);
                mPrintWriter = new PrintWriter(mSocketPrinter.getOutputStream(), true);
                mPrintWriter.print(strOrderId);
                mPrintWriter.flush();

//                BufferedReader input = new BufferedReader(new InputStreamReader(mSocketPrinter.getInputStream()));
//                do {
//                    try {
//                        if (!input.ready()) {
//                            if (message != null) {
//                                mHandler.obtainMessage(0, 0, -1,
//                                        "Server: " + message).sendToTarget();
//                                message = "";
//                            }
//                        }
//                        int num = input.read();
//                        message += Character.toString((char) num);
//                    } catch (Exception classNot) {
//                    }
//
//                } while (!message.equals("bye"));

                try {
                    //sendMessage("bye");
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    mSocketPrinter.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }

        void sendMessage(String msg) {
            try {
//                mPrintWriter.print(msg);
//                mPrintWriter.flush();
//                if (!msg.equals("bye")) {
//                    mHandler.obtainMessage(0, 0, -1, "Me: " + msg).sendToTarget();
//                } else {
//                    mHandler.obtainMessage(0, 0, -1, "Ngắt kết nối!").sendToTarget();
//                }
            } catch (Exception ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
