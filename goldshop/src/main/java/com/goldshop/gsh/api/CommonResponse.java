package com.goldshop.gsh.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CommonResponse {
    public int msgCode;
    public String msgString;
    public String status;
    public Object result;
}
