package com.goldshop.gsh.api;

import com.goldshop.gsh.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class APIClient {
	public static String SERVER = "http://192.168.1.250:8080/" + BuildConfig.HOST + "/";

	public static APIClient getInstance(String IP, int port){
		APIClient client = new APIClient();
		client.SERVER = "http://" + IP + ":" + port + "/" + BuildConfig.HOST + "/";
		return client;
	}

	public static OkHttpClient okHttpClient = new OkHttpClient.Builder()
			.readTimeout(60, TimeUnit.SECONDS)
			.connectTimeout(60, TimeUnit.SECONDS)
			.build();

    public Retrofit getRetrofit() {
		Gson gson = new GsonBuilder()
				.setLenient()
				.create();

		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(SERVER)
				.client(okHttpClient)
				.addConverterFactory(ScalarsConverterFactory.create())
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();
        return retrofit;
    }
}
