package com.goldshop.gsh.api;

import com.goldshop.gsh.models.GoldType;
import com.goldshop.gsh.models.Group;
import com.goldshop.gsh.models.Order;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.models.ProductScanner;
import com.goldshop.gsh.models.ProductSearch;
import com.goldshop.gsh.models.ResultSaveOrder;
import com.goldshop.gsh.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface OrderAPI {

    @POST("Login")
    Call<User> login(@Query("UserName") String userName, @Query("Password") String password);

    @GET("SearchProduct")
    Call<List<ProductSearch>> searchProduct(@Query("PlaceSearch") String placeSearch, @Query("TextSearch") String textSearch, @Query("CodeSearch") String codeSearch);

    @POST("GetProductByCode")
    Call<ProductScanner> getProductByCode(@Query("ProductCode") String ProductCode);

    @POST("GetProductByCodeOLD")
    Call<ProductScanner> getProductByCodeOLD(@Query("ProductCode") String ProductCode);

    @POST("SendPrinter")
    Call<CommonResponse> sendPrinter(@Query("OrderId") long orderId);

    @GET("GetStatusOrder")
    Call<CommonResponse> getStatusOrder(@Query("OrderId") long orderId);

    @POST("UpdateStatusOrder")
    Call<CommonResponse> updateStatusOrder(@Query("OrderId") long orderId, @Query("Status") String status);

    @POST("UpdateInfoPrint")
    Call<CommonResponse> updateInfoPrint(@Query("OrderId") long orderId
            , @Query("SellTotalWeight") int SellTotalWeight
            , @Query("SellTotalWeight_L") int SellTotalWeight_L
            , @Query("SellTotalWeight_C") int SellTotalWeight_C
            , @Query("SellTotalWeight_P") int SellTotalWeight_P
            , @Query("SellTotalWeight_Li") int SellTotalWeight_Li
            , @Query("SellTotalWeightDesc") String SellTotalWeightDesc
            , @Query("BuyTotalWeight") int BuyTotalWeight
            , @Query("BuyTotalWeight_L") int BuyTotalWeight_L
            , @Query("BuyTotalWeight_C") int BuyTotalWeight_C
            , @Query("BuyTotalWeight_P") int BuyTotalWeight_P
            , @Query("BuyTotalWeight_Li") int BuyTotalWeight_Li
            , @Query("BuyTotalWeightDesc") String BuyTotalWeightDesc
            , @Query("TotalWeight") int TotalWeight
            , @Query("TotalWeight_L") int TotalWeight_L
            , @Query("TotalWeight_C") int TotalWeight_C
            , @Query("TotalWeight_P") int TotalWeight_P
            , @Query("TotalWeight_Li") int TotalWeight_Li
            , @Query("TotalWeightDesc") String TotalWeightDesc
            , @Query("PriceSell") double PriceSell
            , @Query("PriceBuy") double PriceBuy
            , @Query("Amount") double Amount
            , @Query("TotalSalary") double TotalSalary
            , @Query("TotalPayBonus") double TotalPayBonus
            , @Query("TotalAmount") double totalAmount);

    @POST("DeleteOrder")
    Call<CommonResponse> deleteOrder(@Query("OrderId") long orderId);

    @FormUrlEncoded
    @POST("SaveOrderDetail")
    Call<ResultSaveOrder> saveOrder(@Field("Method") String Method
            , @Field("TypeTrans") String TypeTrans
            , @Field("ActionTrans") String ActionTrans
            , @Field("OrderId") long OrderId
            , @Field("OrderDetailId") long OrderDetailId
            , @Field("ModUser") String ModUser
            , @Field("TrnID") String TrnID
            , @Field("ProductCode") String ProductCode
            , @Field("ProductDesc") String ProductDesc
            , @Field("DiamondWeight") double DiamondWeight
            , @Field("TotalWeightDT") int TotalWeight
            , @Field("TotalWeightDT_L") int TotalWeightDT_L
            , @Field("TotalWeightDT_C") int TotalWeightDT_C
            , @Field("TotalWeightDT_P") int TotalWeightDT_P
            , @Field("TotalWeightDT_Li") int TotalWeightDT_Li
            , @Field("TotalWeightDTDesc") String TotalWeightDesc
            , @Field("PriceDT") double PriceDT
            , @Field("PayBonus") double PayBonus
            , @Field("PayBonusAmount") double payBonusAmount
            , @Field("Salary") double Salary
            , @Field("AmountDT") double AmountDT
            , @Field("Depot") String Depot
            , @Field("GroupID") String GroupID
            , @Field("GoldType") String GoldType);

    @POST("GetOrderList")
    Call<List<Order>> getOrderList();

    @GET("GetOrderDetails")
    Call<List<OrderDetail>> getOrderDetails(@Query("OrderId") long orderId
            , @Query("OrderDetailId") long orderDetailId
            , @Query("TypeTrans") String typeTrans);

    @POST("GetType")
    Call<List<GoldType>> getGoldType();

    @POST("GetGoldTypeObject")
    Call<GoldType> getGoldTypeObject(@Query("GoldType") String goldType);

    @POST("GetGroup")
    Call<List<Group>> getGroup();



    @GET("CheckGoldTypeExchange")
    Call<CommonResponse> checkExistGoldType(@Query("OrderId") long OrderId, @Query("GoldType") String goldType);
}