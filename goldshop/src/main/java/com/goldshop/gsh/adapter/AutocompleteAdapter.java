package com.goldshop.gsh.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.goldshop.gsh.R;

import java.util.List;

/**
 * Created by HOA on 11/03/2018.
 */

public class AutocompleteAdapter extends ArrayAdapter<String> {
    private List<String> listItems;
    private FragmentActivity mContext;
    private int layoutResourceId;

    public AutocompleteAdapter(@NonNull FragmentActivity context, int layoutResourceId, @NonNull List<String> items) {
        super(context, layoutResourceId, items);

        this.layoutResourceId = layoutResourceId;
        this.listItems = items;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        if (listItems == null){
            return 0;
        } else {
            return listItems.size();
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try{
            if(convertView == null){
                LayoutInflater inflater = mContext.getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }
            String item = listItems.get(position);
            TextView textViewItem = (TextView) convertView.findViewById(R.id.textView1);
            textViewItem.setText(item);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public void setListItems(@NonNull List<String> items){
        this.listItems = items;
        this.notifyDataSetChanged();
    }
}
