package com.goldshop.gsh.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.GoldType;
import com.goldshop.gsh.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

public class GoldTypeAdapter extends RecyclerView.Adapter<GoldTypeAdapter.GoldTypeHolder> {

    private Context context;
    private List<GoldType> goldTypeList;
    private String curGoldType;

    public GoldTypeAdapter(List<GoldType> goldTypes, String curGoldType){
        goldTypeList = goldTypes;
        this.curGoldType = curGoldType;

        if (goldTypeList == null){
            goldTypeList = new ArrayList<>();
        }
        goldTypeList.add(0, new GoldType());
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 1;
        else
            return 2;
    }

    @Override
    public GoldTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gold_type_adapter_holder, parent, false);

        return new GoldTypeHolder(view);
    }

    @Override
    public void onBindViewHolder(GoldTypeHolder holder, int position) {
        if (position == 0){
            holder.view.setBackgroundColor(context.getResources().getColor(R.color.colorOrangePrimary01));
            holder.tvGoldType.setText("Mã");
            holder.tvTypeDesc.setText("Diễn giải");
            holder.tvPriceBuy.setText("G.Mua");
            holder.tvPriceSell.setText("G.Bán");

            holder.view.setEnabled(false);
            return;
        }

        GoldType goldType = goldTypeList.get(position);
        try{
            holder.tvGoldType.setText(goldType.GoldCode);
            holder.tvTypeDesc.setText(goldType.GoldDesc);
            holder.tvPriceBuy.setText(Utilities.trimZeroRight(goldType.PriceBuy));
            holder.tvPriceSell.setText(Utilities.trimZeroRight(goldType.PriceSell));

            try {
                if (goldType.GoldCode.equals(curGoldType)) {
                    holder.view.setBackgroundColor(context.getResources().getColor(R.color.colorOrangePrimary03));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return goldTypeList == null ? 0 : goldTypeList.size();
    }

    public static class GoldTypeHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView tvGoldType, tvTypeDesc, tvPriceSell, tvPriceBuy;

        public GoldTypeHolder(View view) {
            super(view);
            this.view = view;
            tvGoldType = view.findViewById(R.id.tv_gold_type);
            tvTypeDesc = view.findViewById(R.id.tv_gold_type_desc);
            tvPriceSell = view.findViewById(R.id.tv_price_sell);
            tvPriceBuy = view.findViewById(R.id.tv_price_buy);
        }
    }
}
