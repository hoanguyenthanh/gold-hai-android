package com.goldshop.gsh.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.Group;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by HOA on 02/06/2017.
 */

public class InvoiceListGroupAdapter extends BaseAdapter {
    private List<Group> mListGroup = new ArrayList<>();
    private LayoutInflater mInflater;

    public InvoiceListGroupAdapter(Context context, List<Group> list){
        mListGroup = list;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mListGroup.size();
    }

    @Override
    public Group getItem(int position) {
        return mListGroup.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getItemGroupId (int position){
        return getItem(position).GroupID;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.invoice_spinner_item_layout, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_item_text1);
        textView.setText(getItem(position).GroupName);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.invoice_spinner_item_dropdown_layout, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_dropdown_item_text1);
        textView.setText(getItem(position).GroupName);

        if (position % 2 == 0){
            convertView.setBackgroundResource(R.color.bg_spinner_item);
        }

        return convertView;
    }
}
