package com.goldshop.gsh.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.Stalls;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HOA on 02/06/2017.
 */

public class InvoiceListBinAdapter extends BaseAdapter {
    private List<Stalls> mListBin = new ArrayList<>();
    private LayoutInflater mInflater;

    public InvoiceListBinAdapter(Context context){
        mInflater = LayoutInflater.from(context);
        createList();
    }

    @Override
    public int getCount() {
        return mListBin.size();
    }

    @Override
    public Stalls getItem(int position) {
        return mListBin.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mListBin.get(position).getBinId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.invoice_spinner_item_layout, parent, false);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_item_text1);
        textView.setText(mListBin.get(position).getBinName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.invoice_spinner_item_dropdown_layout, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_dropdown_item_text1);
        textView.setText(getItem(position).getBinName());

        if (position % 2 == 0){
            convertView.setBackgroundResource(R.color.bg_spinner_item);
        }

        return convertView;
    }

    private void createList(){
        for(int i = 0; i < 8; i ++){
            Stalls binDto = new Stalls();
            binDto.setBinId(i + 1);
            binDto.setBinName("Kệ " + (i + 1));
            mListBin.add(binDto);
        }
    }
}
