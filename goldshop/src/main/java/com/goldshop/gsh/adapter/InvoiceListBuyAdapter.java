package com.goldshop.gsh.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.utils.Utilities;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.util.ArrayList;
import java.util.List;

public class InvoiceListBuyAdapter extends RecyclerView.Adapter<InvoiceListBuyAdapter.BuyHolder> {
    private List<OrderDetail> listOrderDetail = new ArrayList<>();

    public InvoiceListBuyAdapter(List<OrderDetail> lis){
        listOrderDetail = lis;
    }

    public void updateItem(List<OrderDetail> lis){
        listOrderDetail = lis;
    }

    public OrderDetail getItem(int position) {
        return listOrderDetail.get(position);
    }

    @Override
    public BuyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_list_buy_adapter, parent, false);
        return new BuyHolder(view);
    }

    @Override
    public void onBindViewHolder(BuyHolder holder, int position) {
        OrderDetail detail = listOrderDetail.get(position);
        try {
            holder.txtName.setText(Utilities.replaceStringNull(detail.getProductDesc()) + " [" + detail.getGoldType() + "]");
            holder.txtPrice.setText(Utilities.moneyFormat(detail.getPriceDT()));
            holder.txtPayBonus.setText(Utilities.moneyFormat(detail.getPayBonus()));
            holder.txtPayBonusAmuont.setText(Utilities.moneyFormat(detail.getPayBonusAmount()));
            holder.txtWeight.setText(detail.getTotalWeightDT());
            holder.txtAmount.setText(Utilities.moneyFormat(detail.getAmountDT()));
        }catch (Exception ex){
            Log.e("Exception", ex.toString());
        }
    }

    @Override
    public long getItemId(int position) {
        return listOrderDetail.get(position).getOrderId();
    }

    @Override
    public int getItemCount() {
        if (listOrderDetail == null) {
            return 0;
        }
        return listOrderDetail.size();
    }

    class BuyHolder extends RecyclerView.ViewHolder{
        TextView txtName;
        WeightGoldView txtWeight;
        TextView txtPrice;
        TextView txtPayBonus;
        TextView txtPayBonusAmuont;
        TextView txtAmount;

        public BuyHolder(View convertView) {
            super(convertView);
            txtName = (TextView)convertView.findViewById(R.id.invoice_list_buy_adapter_txt_name);
            txtWeight = (WeightGoldView)convertView.findViewById(R.id.invoice_list_buy_adapter_txt_weight);
            txtPrice = (TextView)convertView.findViewById(R.id.invoice_list_buy_adapter_txt_price);
            txtPayBonus = (TextView)convertView.findViewById(R.id.invoice_list_buy_adapter_txt_pay_bonus);
            txtPayBonusAmuont = (TextView)convertView.findViewById(R.id.invoice_list_buy_adapter_txt_pay_bonus_amount);
            txtAmount = (TextView) convertView.findViewById(R.id.invoice_list_buy_adapter_txt_amount);
        }
    }
}
