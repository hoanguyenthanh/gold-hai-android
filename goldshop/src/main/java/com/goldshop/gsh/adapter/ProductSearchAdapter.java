package com.goldshop.gsh.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.SearchProductFragment;
import com.goldshop.gsh.models.ProductSearch;

import java.util.List;

/**
 * Created by HOA on 17/03/2018.
 */

public class ProductSearchAdapter extends RecyclerView.Adapter<ProductSearchAdapter.SearchHolder>{
    private List<ProductSearch> searchList;
    private String typeSearch = "new";

    public ProductSearchAdapter(List<ProductSearch> list){
        searchList = list;
    }


    public void setTypeSearch(String type){
        typeSearch = type;
    }

    public void updateItem(List<ProductSearch> list){
        searchList = list;
        notifyDataSetChanged();
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_code_item_holder, parent, false);
        return new SearchHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchHolder holder, int position) {
        ProductSearch codeSearch = searchList.get(position);

        try {
            if (TextUtils.isEmpty(codeSearch.GoldCode)) {
                holder.txtCode.setText(" - ");
            } else {
                holder.txtCode.setText(codeSearch.GoldCode);
            }

            if (TextUtils.isEmpty(codeSearch.ProductDesc)) {
                holder.txtDesc.setText(" - ");
            } else {
                holder.txtDesc.setText(codeSearch.ProductDesc);
            }


            if (SearchProductFragment.TYPE_SEARCH_OLD.equals(typeSearch)) {
                String weight = String.format("%.3f", codeSearch.TotalWeight);
                if (weight != null) {
                    weight = weight.replace(",", ".");
                }
                holder.txtWeight.setText(weight);
            } else {
                holder.txtWeight.setText(codeSearch.TotalWeight + "");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        if (searchList == null) {
            return 0;
        }
        return searchList.size();
    }

    class SearchHolder extends RecyclerView.ViewHolder {
        TextView txtCode, txtDesc, txtWeight;
        public SearchHolder(View v) {
            super(v);
            txtCode = v.findViewById(R.id.tv_gold_code);
            txtDesc = v.findViewById(R.id.tv_desc);
            txtWeight = v.findViewById(R.id.tv_weight);
        }
    }
}
