package com.goldshop.gsh.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.GoldType;

import java.util.ArrayList;
import java.util.List;

public class InvoiceListTypeAdapter extends BaseAdapter {
    private List<GoldType> mListType  = new ArrayList<>();
    private LayoutInflater mInflater;

    public InvoiceListTypeAdapter(Context context, List<GoldType> list){
        mListType = list;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mListType.size();
    }

    @Override
    public GoldType getItem(int position) {
        return mListType.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public String getItemTypeId(int position){
        return getItem(position).GoldCode;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = mInflater.inflate(R.layout.invoice_spinner_item_layout, parent, false);
        }
        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_item_text1);
        textView.setText(getItem(position).GoldDesc);

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.invoice_spinner_item_dropdown_layout, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.invoice_spinner_dropdown_item_text1);
        textView.setText(getItem(position).GoldDesc);

        if (position % 2 == 0){
            convertView.setBackgroundResource(R.color.bg_spinner_item);
        }
        return convertView;
    }
}
