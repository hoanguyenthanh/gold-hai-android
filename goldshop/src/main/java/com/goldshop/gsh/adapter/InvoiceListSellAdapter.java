package com.goldshop.gsh.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.utils.Utilities;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.util.ArrayList;
import java.util.List;

public class InvoiceListSellAdapter extends RecyclerView.Adapter<InvoiceListSellAdapter.SellHolder> {
    private List<OrderDetail> listOrderDetail= new ArrayList<OrderDetail>();


    public InvoiceListSellAdapter(List<OrderDetail> lis){
        this.listOrderDetail = lis;
    }

    public void updateItem(List<OrderDetail> lis){
        listOrderDetail = lis;
    }

    public OrderDetail getItem(int position) {
        return listOrderDetail.get(position);
    }

    @Override
    public SellHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_list_sell_adapter, parent, false);
        return new SellHolder(view);
    }

    @Override
    public void onBindViewHolder(SellHolder holder, int position) {
        OrderDetail detail = getItem(position);
        try {
            holder.txtName.setText(Utilities.replaceStringNull(detail.getProductDesc()));
            holder.txtWeight.setText(detail.getTotalWeightDT());
            holder.txtPrice.setText(Utilities.moneyFormat(detail.getPriceDT()));
            holder.txtSalary.setText(Utilities.moneyFormat(detail.getSalary()));
            holder.txtAmount.setText(Utilities.moneyFormat(detail.getAmountDT()));


            String goldTypeDesc = detail.getGoldType() + ": " + detail.getGoldDesc();
            holder.txtGoldType.setText(goldTypeDesc);
        }catch (Exception e){
            Log.e("Exception", e.toString());
        }

    }

    @Override
    public long getItemId(int position) {
        return listOrderDetail.get(position).getOrderId();
    }

    @Override
    public int getItemCount() {
        if (listOrderDetail == null) {
            return 0;
        }
        return listOrderDetail.size();
    }

    class SellHolder extends RecyclerView.ViewHolder{
        WeightGoldView txtWeight;
        TextView txtName;
        TextView txtPrice;
        TextView txtSalary;
        TextView txtAmount;
        TextView txtGoldType;

        public SellHolder(View convertView) {
            super(convertView);
            txtName = convertView.findViewById(R.id.invoice_list_sell_adapter_txt_name);
            txtPrice = convertView.findViewById(R.id.invoice_list_sell_adapter_txt_price);
            txtSalary = convertView.findViewById(R.id.invoice_list_sell_adapter_txt_salary);
            txtWeight = convertView.findViewById(R.id.invoice_list_sell_adapter_txt_weight);
            txtAmount = convertView.findViewById(R.id.invoice_list_sell_adapter_txt_amount);
            txtGoldType = convertView.findViewById(R.id.tv_gold_type);
        }
    }
}
