package com.goldshop.gsh.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.models.Order;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by HOA on 03/03/2018.
 */
public class ListInvoiceAdapter extends RecyclerView.Adapter<ListInvoiceAdapter.InvoiceHolder> {
    public static final String TAG = ListInvoiceAdapter.class.getSimpleName();

    public static final int VIEW_TYPE_HEADER = 1;
    public static final int VIEW_TYPE_BODY = 2;



    private List<Order> listOrder;
    private DecimalFormat mFormat;

    public ListInvoiceAdapter(List<Order> orderList) {

        listOrder = orderList;
        if (listOrder == null) {
            listOrder = new ArrayList<>();
        }
        listOrder.add(0, new Order());

        mFormat = new DecimalFormat("###,###,###");
    }

    public void setListOrder(List<Order> orderList){
        listOrder = orderList;
        if (listOrder == null) {
            listOrder = new ArrayList<>();
        }
        listOrder.add(0, new Order());

        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position <= 0)
            return VIEW_TYPE_HEADER;
        else
            return VIEW_TYPE_BODY;
    }

    @Override
    public InvoiceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_invoice_adapter, parent, false);
        return new InvoiceHolder(itemView, viewType);
    }

    @Override
    public void onBindViewHolder(InvoiceHolder holder, int position) {
        if (position <= 0) return;

        Order order = listOrder.get(position);
        try {

            if (Constants.STATUS_NEW.equals(order.Status)) {
                holder.setBackgroundStatusNew(R.drawable.bg_line_bottom_gray01);
            } else {
                holder.setBackgroundStatusNew(R.drawable.bg_line_bottom_gray);
            }

            int totalWeight = order.getTotalWeight();
            holder.txtWeight.setVisibility(View.VISIBLE);
            holder.txtWeight.setText(totalWeight);

            holder.txtDescProduct.setText(String.valueOf(order.ProductDesc));
            holder.txtStatus.setText(getStatusName(order.Status));

            if (order.getTotalAmount() == 0){
                holder.txtAmount.setText(Constants.HYPHEN);
            } else {
                holder.txtAmount.setText(mFormat.format(order.getTotalAmount()));
            }
            holder.txtDate.setText(order.getModDate());
            holder.txtTime.setText(order.getModTime());
            holder.txtTrans.setText(getTransName(order.getTypeTrans()));
            holder.txtUser.setText(order.getModUser());
        }catch (Exception ex){
            Log.e(TAG, "onBindViewHolder error: " + ex.getLocalizedMessage());
        }
    }

    @Override
    public int getItemCount() {
        if (listOrder == null){
            return 0;
        }
        return this.listOrder.size();
    }


    public class InvoiceHolder extends RecyclerView.ViewHolder {
        protected TextView txtDescProduct, txtAmount, txtDate, txtTime, txtTrans, txtStatus, txtUser, txtTotalWeight;
        protected WeightGoldView txtWeight;
        private View viewRoot;

        public InvoiceHolder(View convertView, int viewType) {
            super(convertView);

            txtDescProduct = convertView.findViewById(R.id.list_invoice_adapter_txt_desc);
            txtAmount = convertView.findViewById(R.id.list_invoice_adapter_txt_amount);
            txtDate = convertView.findViewById(R.id.list_invoice_adapter_txt_date);
            txtTime= convertView.findViewById(R.id.list_invoice_adapter_txt_time);
            txtTrans = convertView.findViewById(R.id.list_invoice_adapter_txt_trans);
            txtStatus = convertView.findViewById(R.id.list_invoice_adapter_txt_status);
            txtUser = convertView.findViewById(R.id.list_invoice_adapter_txt_user);
            txtWeight = convertView.findViewById(R.id.list_invoice_adapter_txt_weight);
            txtTotalWeight = convertView.findViewById(R.id.tvTotalWeight);

            if (viewType == VIEW_TYPE_HEADER){
                txtTotalWeight.setVisibility(View.VISIBLE);
                txtWeight.setVisibility(View.GONE);

                txtDescProduct.setText("Diễn giải");
                txtAmount.setText("T.Tiền");
                txtDate.setText("Ngày");
                txtTime.setText("Giờ");
                txtTrans.setText("Giao dịch");
                txtStatus.setText("T.Thái");
                txtUser.setText("N.Viên");

                if (convertView.getContext() != null && convertView.getResources() != null){
                    convertView.setBackgroundColor(convertView.getResources().getColor(R.color.colorGray));
                }
            } else {
                txtDescProduct.setText(Constants.HYPHEN);
                txtAmount.setText(Constants.HYPHEN);
                txtDate.setText(Constants.HYPHEN);
                txtTime.setText(Constants.HYPHEN);
                txtTrans.setText(Constants.HYPHEN);
                txtStatus.setText(Constants.HYPHEN);
                txtUser.setText(Constants.HYPHEN);

                txtTotalWeight.setVisibility(View.GONE);
                txtWeight.setVisibility(View.VISIBLE);
            }

            viewRoot = convertView;
        }

        public void setBackgroundStatusNew(int drawId){
            if (viewRoot != null && viewRoot.getContext() != null){
                viewRoot.setBackground(viewRoot.getContext().getResources().getDrawable(drawId));
            }
        }
    }

    private String getTransName(String value){
        if (value == null){
            return "-";
        }
        else if (value.equals(Constants.BUY)){
            return "Mua";
        }
        else if (value.equals(Constants.SELL)){
            return "Bán";
        }
        else if (value.equals(Constants.SELL_BUY)){
            return "Mua-Bán";
        }
        return "-";
    }

    private String getStatusName(String value){
        if (value == null){
            return "-";
        }
        else if (value.equals(Constants.STATUS_NEW)){
            return "Mới";
        }
        else if (value.equals(Constants.STATUS_PRINTED)){
            return "Đã in";
        }
        else if (value.equals(Constants.STATUS_CANCELED)){
            return "Đã hủy";
        }
        else if (value.equals(Constants.STATUS_PAID)){
            return "Paid";
        }
        return "-";
    }
}
