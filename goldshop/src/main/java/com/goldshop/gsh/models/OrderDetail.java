package com.goldshop.gsh.models;

public class OrderDetail {
    public long OrderDetailId;
    public long OrderId;
    public String ModUser;
    public String ModDt;
    public int Shared;
    public String TrnID;
    public String ProductCode;
    public String ProductDesc;
    public double DiamondWeight;
    public int TotalWeightDT;
    public double PriceDT;
    public double PayBonus;
    public double PayBonusAmount;
    public double Salary;
    public double AmountDT;
    public String GroupID;
    public String GroupName;
    public String GoldType;
    public String GoldDesc;
    public String Depot;
    public String TypeTrans;

    public String getDepot() {
        return Depot;
    }

    public double getDiamondWeight() {
        return DiamondWeight;
    }

    public String getGoldType() {
        return GoldType;
    }

    public String getGoldDesc() {
        return GoldDesc;
    }

    public String getGroupID() {
        return GroupID;
    }

    public String getGroupName() {
        return GroupName;
    }

    public String getModDt() {
        return ModDt;
    }

    public String getModUser() {
        return ModUser;
    }

    public long getOrderDetailId() {
        return OrderDetailId;
    }

    public long getOrderId() {
        return OrderId;
    }

    public double getPayBonus() {
        return PayBonus;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public String getProductDesc() {
        return ProductDesc;
    }

    public double getSalary() {
        return Salary;
    }

    public double getPayBonusAmount() {
        return PayBonusAmount;
    }

    public int getShared() {
        return Shared;
    }

    public int getTotalWeightDT() {
        return TotalWeightDT;
    }

    public String getTrnID() {
        return TrnID;
    }

    public String getTypeTrans() {
        return TypeTrans;
    }

    public double getAmountDT() {
        return AmountDT;
    }

    public double getPriceDT() {
        return PriceDT;
    }
}
