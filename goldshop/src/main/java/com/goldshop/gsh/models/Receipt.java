package com.goldshop.gsh.models;

/**
 * Created by HOA on 01/05/2017.
 */

public class Receipt {
    public String id;
    public String productCode;
    public String productName;
    public String productNumber;
    public String productPrice;
    public String productAmount;

    public String customerNumber;
    public String customerPrice;
    public String customerAmount;

    public String diffNumber;
    public String diffPrice;
    public String diffAmount;

    public String salaryNumber;
    public String salaryPrice;
    public String salaryAmount;
}
