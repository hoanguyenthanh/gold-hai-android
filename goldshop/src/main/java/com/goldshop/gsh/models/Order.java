package com.goldshop.gsh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {
    public long OrderId;
    public String ProductDesc;
    public String TypeTrans;
    public String Status;
    public String StatusName;
    public String ModUser;
    public String ModDt;
    public String ModDate;
    public String ModTime;
    public double TotalAmount;
    public int TotalWeight;

    public String getTypeTrans(){
        return TypeTrans;
    }

    public String getModDt() {
        return ModDt;
    }

    public String getModDate() {
        return ModDate;
    }

    public String getModTime() {
        return ModTime;
    }

    public String getModUser() {
        return ModUser;
    }

    public long getOrderId() {
        return OrderId;
    }

    public String getStatus() {
        return Status;
    }

    public String getStatusName() {
        return StatusName;
    }

    public double getTotalAmount() {
        return TotalAmount;
    }

    public int getTotalWeight(){
        return TotalWeight;
    }
}
