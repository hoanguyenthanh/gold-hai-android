package com.goldshop.gsh.models;

/**
 * Created by HOA on 02/06/2017.
 */

public class Stalls {
    int binId;
    String binName;

    public int getBinId() {
        return binId;
    }

    public void setBinId(int binId) {
        this.binId = binId;
    }

    public String getBinName() {
        return binName;
    }

    public void setBinName(String binName) {
        this.binName = binName;
    }
}
