package com.goldshop.gsh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by HOA on 02/06/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Group {
    public String GroupID;
    public String GroupCode;
    public String GroupName;
}
