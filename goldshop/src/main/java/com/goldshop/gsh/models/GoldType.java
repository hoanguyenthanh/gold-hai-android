package com.goldshop.gsh.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;


public class GoldType {
    public String GoldCode;
    public String GoldDesc;
    public String GoldType;
    public BigDecimal HS;
    public double PriceSell;
    public double PriceBuy;
}
