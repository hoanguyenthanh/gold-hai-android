package com.goldshop.gsh.models;

/**
 * Created by HOA on 01/05/2017.
 */

public class Product {
    public int id;
    public String code;
    public String name;
    public String type;
    public String salary;
    public String weight;
}
