package com.goldshop.gsh.models;

/**
 * Created by HOA on 06/08/2017.
 */

public class ProductScanner {
    public String ProductCode;
    public String ProductDesc;
    public String GoldCode;
    public String GoldDesc;
    public String GroupID;
    public String GroupName;
    public double GoldWeight;
    public double DiamondWeight;
    public double TotalWeight;
}
