package com.goldshop.gsh.models;

/**
 * Created by HOA on 31/07/2018.
 */

public class User {
    public long UserID;
    public String UserName;
    public String Password;
    public String FullName;
    public String Email;
    public String Mobile;
    public String LastVisited;
    public int IsAdmin;
    public int Active;


    public boolean isAdmin(){
        return IsAdmin == 1;
    }
}
