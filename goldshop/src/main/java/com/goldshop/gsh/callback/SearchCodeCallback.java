package com.goldshop.gsh.callback;

import com.goldshop.gsh.models.ProductSearch;

/**
 * Created by HOA on 18/03/2018.
 */

public interface SearchCodeCallback {
    void selectData(ProductSearch codeSearch);
}
