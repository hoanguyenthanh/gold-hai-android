package com.goldshop.gsh.callback;

public interface AcceptCallback {
    void accept();
}
