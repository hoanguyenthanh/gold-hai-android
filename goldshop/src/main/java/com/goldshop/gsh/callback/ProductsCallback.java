package com.goldshop.gsh.callback;

import com.goldshop.gsh.models.OrderDetail;

/**
 * Created by HOA on 02/08/2018.
 */

public interface ProductsCallback {
    void itemClick(OrderDetail orderDetail);
    void addClick(String action);
    void chargingClick();
    void startCamera(boolean isStart);
}
