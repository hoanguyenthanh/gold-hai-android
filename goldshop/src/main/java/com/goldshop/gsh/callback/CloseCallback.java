package com.goldshop.gsh.callback;

/**
 * Created by HOA on 31/07/2018.
 */

public interface CloseCallback {
    void onClose();
}
