package com.goldshop.gsh.callback;

/**
 * Created by HOA on 04/08/2018.
 */

public interface OnPagingListener {
    void setPageIndex(int pageIndex);
}
