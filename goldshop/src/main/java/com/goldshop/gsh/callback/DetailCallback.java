package com.goldshop.gsh.callback;

public interface DetailCallback {
    void saveSuccess(long orderId, long orderDetailId);
    void chargingClick();
}
