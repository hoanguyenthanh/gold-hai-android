package com.goldshop.gsh.callback;

import android.view.View;

/**
 * Created by HOA on 04/08/2018.
 */

public interface ButtonDeleteCallback {
    void deleteClicked(View view);
}
