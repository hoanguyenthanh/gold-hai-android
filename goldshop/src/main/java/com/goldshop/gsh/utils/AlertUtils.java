package com.goldshop.gsh.utils;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.callback.AcceptCallback;
import com.goldshop.gsh.callback.CloseCallback;
import com.goldshop.gsh.models.ResponseError;
import com.google.gson.Gson;

import okhttp3.ResponseBody;

/**
 * Created by HOA on 31/07/2018.
 */

public class AlertUtils {

    public static AlertDialog getAlertClose(Context context, String title, String message, CloseCallback callback){
        return new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setNegativeButton("Đóng", (dialogInterface, i) -> {
                if (callback != null) callback.onClose();
            })
            .create();
    }

    public static void showAlertYesNo(){

    }

    public static AlertDialog getAlertOKCancel(Context context, String title, String msg
            , CloseCallback closeCallback, AcceptCallback acceptCallback){

        View view = LayoutInflater.from(context).inflate(R.layout.alert_yes_no_layout, null);
        AlertDialog alert = new AlertDialog.Builder(context)
                .setView(view)
                .create();

        TextView tvTitle = view.findViewById(R.id.tv_title);
        TextView tvMessage = view.findViewById(R.id.tv_message);
        TextView tvOk = view.findViewById(R.id.tv_ok);
        TextView tvClose = view.findViewById(R.id.tv_close);

        tvTitle.setText(title == null ? "" : title);
        tvMessage.setText(msg == null ? "" : msg);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alert != null && alert.isShowing()){
                    alert.dismiss();
                }

                if (acceptCallback != null){
                    acceptCallback.accept();
                }

            }
        });

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (alert != null && alert.isShowing()){
                    alert.dismiss();
                }

                if (closeCallback != null){
                    closeCallback.onClose();
                }
            }
        });

        return alert;
    }

    public static void showToastLong(Context context, String message){
        try {
            if (message == null) message = "";
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void showToastShort(Context context, String message){
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void showErrorResponse(Context context, ResponseBody responseBody) {
        String message = "Error";

        try {
            ResponseBody body = Utilities.cloneResponseBody(responseBody);
            ResponseError apiErrorPayment = null;
            Gson gson = new Gson();
            apiErrorPayment = gson.fromJson(body.string(), ResponseError.class);


            if (apiErrorPayment != null && apiErrorPayment.message != null && !apiErrorPayment.message.isEmpty())
                message = apiErrorPayment.message;
            else
                message = context.getString(R.string.str_unknow_message);
        } catch (Exception e) {
            message = e.getLocalizedMessage();
        } finally {
            if (context != null){
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static String getMessageThrowable(Context context, Throwable throwable) {
        String message;
        try {
            if (!Utilities.isNetworkAvailable(context)) {
                message = context.getString(R.string.str_no_connect_internet);
            } else if (throwable != null && throwable.getMessage() != null) {
                message = throwable.getMessage();
            } else {
                message = context != null ? context.getString(R.string.str_unknow_message) : "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = e.getLocalizedMessage();
        }

        return message;
    }
}
