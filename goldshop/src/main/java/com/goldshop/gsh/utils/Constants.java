package com.goldshop.gsh.utils;

/**
 * Created by DLV-LAP-37 on 16/06/2016.
 *
 */
public class Constants {
    public static final String STATUS_NEW = "new";
    public static final String STATUS_PRINTED = "printed";
    public static final String STATUS_CANCELED = "canceled";
    public static final String STATUS_PAID = "paid";

    public static final String SELL = "sell";
    public static final String BUY = "buy";
    public static final String SELL_BUY = "sell_and_buy";

    public static final String EXTRA_NAME_CLASS = "EXTRA_NAME_CLASS";
    public static final String EXTRA_TYPE_TRANS = "EXTRA_TYPE_TRANS";
    public static final String EXTRA_ORDER_ID = "EXTRA_ORDER_ID";

    public static final String WEIGHT_SYMBOL_LUONG = "l";
    public static final String WEIGHT_SYMBOL_CHI = "c";
    public static final String WEIGHT_SYMBOL_PHAN = "p";
    public static final String WEIGHT_SYMBOL_LI = "li";

    public static final String HYPHEN = "-";
    public static final String ZERO = "0";
    public static final String EMPTY = "";

    public static final String METHOD_INSERT = "insert";
    public static final String METHOD_UPDATE = "update";
    public static final String METHOD_DELETE = "delete";

    //Scanner
    public static final int ACTIVITY_CODE_RESULT_SCANNER = 2017;

    public static final String EXTRA_CODE_PRODUCT = "EXTRA_CODE_PRODUCT";
    public static final String EXTRA_CODE_FORMAT = "EXTRA_CODE_FORMAT";
}
