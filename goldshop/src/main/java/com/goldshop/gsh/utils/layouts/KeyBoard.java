package com.goldshop.gsh.utils.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldshop.gsh.R;


public class KeyBoard extends LinearLayout implements View.OnClickListener{
    private View mView;
    private long mTargetId;

    private TextView btn1;
    private TextView btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnPoint;
    private LinearLayout btnBackspace;
    private TextView  btnClear, btnAccept, btnClose;

    private OnListener onListener;
    private StringBuilder txtValue = new StringBuilder();
    private String valueBackup = "";

    public KeyBoard(Context context) {
        super(context);
        setView(context, null, 0);
    }

    public KeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
        setView(context, attrs, 0);
    }

    public KeyBoard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setView(context, attrs, defStyleAttr);
    }

    public void setTargetId(long id, String value){
        this.mTargetId = id;

        this.txtValue.setLength(0);
        this.txtValue.append(value);
        this.valueBackup = value;
    }

    public long getTargetId(){
        return this.mTargetId;
    }

    private void setView(Context context, AttributeSet attributeSet, int resId){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.key_board_layout, this, true);

        btn1 = mView.findViewById(R.id.txt_keyboard_number_1);
        btn2 = mView.findViewById(R.id.txt_keyboard_number_2);
        btn3 = mView.findViewById(R.id.txt_keyboard_number_3);
        btn4 = mView.findViewById(R.id.txt_keyboard_number_4);
        btn5 = mView.findViewById(R.id.txt_keyboard_number_5);
        btn6 = mView.findViewById(R.id.txt_keyboard_number_6);
        btn7 = mView.findViewById(R.id.txt_keyboard_number_7);
        btn8 = mView.findViewById(R.id.txt_keyboard_number_8);
        btn9 = mView.findViewById(R.id.txt_keyboard_number_9);
        btn0 = mView.findViewById(R.id.txt_keyboard_number_0);
        btnPoint = mView.findViewById(R.id.txt_keyboard_point);

        btnClear = mView.findViewById(R.id.txt_keyboard_clear);
        btnBackspace = mView.findViewById(R.id.linear_keyboard_backspace);
        btnAccept = mView.findViewById(R.id.btn_keyboard_accept);
        btnClose = mView.findViewById(R.id.txt_keyboard_number_cancel);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btn0.setOnClickListener(this);
        btnPoint.setOnClickListener(this);

        btnClear.setOnClickListener(this);
        btnBackspace.setOnClickListener(this);
        mView.findViewById(R.id.btn_keyboard_backspace).setOnClickListener(this);
        btnAccept.setOnClickListener(this);
        btnClose.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (txtValue.toString().length() > 10){
            if (v.getId() == R.id.linear_keyboard_backspace || v.getId() == R.id.btn_keyboard_backspace){
                String value = txtValue.toString().substring(0, txtValue.toString().length() - 1);
                txtValue.setLength(0);
                txtValue.append(value);
                onListener.setValueChanged(this.mTargetId, txtValue.toString());
            }
            else if (v.getId() == R.id.txt_keyboard_clear){
                txtValue.setLength(0);
                onListener.setValueChanged(this.mTargetId, txtValue.toString());
            }
            else if (v.getId() == R.id.txt_keyboard_number_cancel){
                onListener.setValueChanged(this.mTargetId, valueBackup);
                valueBackup = "";
                txtValue.setLength(0);
                this.setVisibility(View.GONE);
            }
            return;
        }

        switch (v.getId())
        {
            case R.id.txt_keyboard_number_1:
                txtValue.append("1");
                break;
            case R.id.txt_keyboard_number_2:
                txtValue.append("2");
                break;
            case R.id.txt_keyboard_number_3:
                txtValue.append("3");
                break;
            case R.id.txt_keyboard_number_4:
                txtValue.append("4");
                break;
            case R.id.txt_keyboard_number_5:
                txtValue.append("5");
                break;
            case R.id.txt_keyboard_number_6:
                txtValue.append("6");
                break;
            case R.id.txt_keyboard_number_7:
                txtValue.append("7");
                break;
            case R.id.txt_keyboard_number_8:
                txtValue.append("8");
                break;
            case R.id.txt_keyboard_number_9:
                txtValue.append("9");
                break;
            case R.id.txt_keyboard_number_0:
                txtValue.append("0");
                break;
            case R.id.txt_keyboard_point:
                if (txtValue.toString().length() > 0 && !txtValue.toString().contains(".")){
                    txtValue.append(".");
                }
                break;
            case R.id.txt_keyboard_clear:
                txtValue.setLength(0);
                break;
            case R.id.btn_keyboard_accept:
                this.setVisibility(View.GONE);
                break;
            case R.id.txt_keyboard_number_cancel:
                this.setVisibility(View.GONE);
                break;
            case R.id.linear_keyboard_backspace:case R.id.btn_keyboard_backspace:
                if (txtValue.length() > 0){
                    String value = txtValue.toString().substring(0, txtValue.toString().length() - 1);
                    txtValue.setLength(0);
                    txtValue.append(value);
                }
                break;
        }


        if (v.getId() == R.id.btn_keyboard_accept){
            onListener.setValueChanged(this.mTargetId, txtValue.toString());
            onListener.onCloseWindow(0);
            txtValue.setLength(0);
        }
        else if (v.getId() == R.id.txt_keyboard_number_cancel) {
            onListener.setValueChanged(this.mTargetId, valueBackup);
            onListener.onCloseWindow(0);
            valueBackup = "";
            txtValue.setLength(0);
        }
        else{
            onListener.setValueChanged(this.mTargetId, txtValue.toString());
        }
    }

    public interface OnListener{
        void setValueChanged(long id, String value);
        void onCloseWindow(long id);
    }

    public void setOnListener(OnListener listener){
        this.onListener = listener;
    }
}
