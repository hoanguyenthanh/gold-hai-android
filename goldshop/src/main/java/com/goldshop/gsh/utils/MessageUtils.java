package com.goldshop.gsh.utils;

@Deprecated
public class MessageUtils {

    /*
    public static String getMessageThrowable(Throwable throwable, Context context) {
        String message;
        try {
            if (!Utilities.isNetworkAvailable(context)) {
                message = context.getString(R.string.str_no_connect_internet);
            } else if (throwable != null && throwable.getMessage() != null) {
                message = throwable.getMessage();
            } else {
                message = context != null ? context.getString(R.string.str_unknow_message) : "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = e.getLocalizedMessage();
        }

        return message;
    }


    public static void showErrorReponse(ResponseBody responseBody, int code, Context context) {
        String message = "Error";

        try {
            ResponseBody body = Utilities.cloneResponseBody(responseBody);
            ResponseError apiErrorPayment = null;
            Gson gson = new Gson();
            apiErrorPayment = gson.fromJson(body.string(), ResponseError.class);


            if (apiErrorPayment != null && apiErrorPayment.message != null && !apiErrorPayment.message.isEmpty())
                message = apiErrorPayment.message;
            else
                message = context.getString(R.string.str_unknow_message);
        } catch (Exception e) {
            message = e.getLocalizedMessage();
        } finally {
            if (context != null){
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        }
    }


    public static String getString(ResponseBody responseBody, Activity activity) {
        ResponseError apiErrorPayment = null;
        ResponseBody body = Utilities.cloneResponseBody(responseBody);
        Gson gson = new Gson();
        try {
            if (body == null){
                return activity.getString(R.string.str_unknow_message);
            }

            apiErrorPayment = gson.fromJson(body.string(), ResponseError.class);
            String message = "";
            if (apiErrorPayment != null && apiErrorPayment.getMessage() != null && !apiErrorPayment.getMessage().isEmpty())
                message = apiErrorPayment.getMessage();
            else
                message = activity.getString(R.string.str_unknow_message);
            return message;
        } catch (Exception e) {
            if (activity != null) {
                return activity.getString(R.string.str_unknow_message);
            } else {
                return "";
            }
        }

    }



    public static void showMessageCast(ResponseBody responseBody, int code, Context activity) {
        ResponseBody body = Utilities.cloneResponseBody(responseBody);
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();
        try {
            if (body == null){
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }

            apiErrorPayment = gson.fromJson(body.string(), ResponseError.class);
            Toast.makeText(activity, apiErrorPayment.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("--status--", "status: " + code);
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static void showLoginErrorDialog(ResponseError responseError, AppCompatActivity activity) {
        if (activity == null){
            return;
        }
        if (responseError == null || TextUtils.isEmpty(responseError.getMessage())) {
            Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
        } else {
            DialogUtils.alertDialogOneButton(activity, activity.getString(R.string.login_error), responseError.getMessage(), activity.getString(R.string.confirm_ok_button), null).show();
        }
    }


    public static void showMessageUnknow(Throwable throwable, Activity activity) {
        try {
            if (Utilities.isNetworkAvailable(activity)) {
                Toast.makeText(activity, activity.getString(R.string.str_unknow_message), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, activity.getString(R.string.str_message_network), Toast.LENGTH_SHORT).show();
            }
            if (throwable != null)
                Log.e("showMessageUnknow", "showMessageUnknow: " + throwable.getMessage(), throwable);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showMessageUnknow(Throwable throwable, Context activity) {
        try {
            if (Utilities.isNetworkAvailable(activity)) {
                Toast.makeText(activity, activity.getString(R.string.str_unknow_message), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, activity.getString(R.string.str_message_network), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static void showMessageContent(ResponseBody responseBody, int code, Activity activity) {
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();

        try {
            ResponseBody body = Utilities.cloneResponseBody(responseBody);
            apiErrorPayment = gson.fromJson(body.string(), ResponseError.class);
            Toast.makeText(activity, apiErrorPayment.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("--status--", "status: " + code);
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }
        }

    }

    public static void showMessageContent(ResponseBody responseBody, int code, Context activity) {
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();
        try {
            apiErrorPayment = gson.fromJson(responseBody.string(), ResponseError.class);
            Toast.makeText(activity, apiErrorPayment.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("--status--", "status: " + code);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showMessageBilling(ResponseBody responseBody, int code, Activity activity) {
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();
        try {
            apiErrorPayment = gson.fromJson(responseBody.string(), ResponseError.class);
            Toast.makeText(activity, apiErrorPayment.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void showMessagepayment(ResponseBody responseBody, int code, AppCompatActivity activity) {
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();
        try {
            String message = "";
            apiErrorPayment = gson.fromJson(responseBody.string(), ResponseError.class);
            if (apiErrorPayment != null && apiErrorPayment.getMessage() != null && !
                    apiErrorPayment.getMessage().isEmpty()) {
                message = apiErrorPayment.getMessage();
            } else {
                message = activity.getString(R.string.str_unknow_message);
            }
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
//            Log.e("--status--", "status: " + code);
//            showAlert(activity, apiErrorPayment.getMessage(), apiErrorPayment.getMessage() + "/" + code + "/" + apiErrorPayment.getError());
        } catch (Exception e) {
            e.printStackTrace();
            if (activity != null) {
                Toast.makeText(activity, activity.getString(R.string.str_error_message), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static void showMessagepayment(ResponseBody responseBody, int code, Context activity) {
        ResponseError apiErrorPayment = null;
        Gson gson = new Gson();
        try {
            apiErrorPayment = gson.fromJson(responseBody.string(), ResponseError.class);
            Toast.makeText(activity, apiErrorPayment.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e("--status--", "status: " + code);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showToastNetwork(Activity activity) {
        if (activity != null) {
            Toast.makeText(activity, activity.getString(R.string.str_message_network), Toast.LENGTH_SHORT).show();
        }
    }

    public static void showAlert(final Activity mAppActivity, String title, final String des) {
        new AlertDialog.Builder(mAppActivity)
                .setTitle(title)
                .setMessage(des)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        String shareBody = des;
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                        mAppActivity.startActivity(Intent.createChooser(sharingIntent, mAppActivity.getResources().getString(R.string.str_actor)));
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public static void showWelcomeMsg(BaseActivity activity, String msg, int type, OnClose closeCallback) {
        if (activity == null){
            return;
        }

        if (msg == null) {
            msg = "";
        }
        String strName = activity.getString(R.string.user_name);

        // Toast type, case log in
        if (type == 0) {
            SFUtils sfUtils = new SFUtils(activity);
            if (sfUtils.getUserInfo() != null) {
                UserInfo.UserProfileEntity userProfileEntity = sfUtils.getUserInfo().getUser_profile();
                if (userProfileEntity != null) {
                    if (userProfileEntity.getFullName() != null &&
                            !userProfileEntity.getFullName().isEmpty()) {
                        strName = userProfileEntity.getFullName();
                    } else if (userProfileEntity.getLocalAcc().getMobile() != null &&
                            !userProfileEntity.getLocalAcc().getMobile().isEmpty()) {
                        strName = userProfileEntity.getLocalAcc().getMobile();
                    }
                }
            }
            String strTitle = String.format(activity.getString(R.string.str_welcome), strName);
            activity.showMessage(strTitle, msg, R.drawable.ic_login_success);
        } else {
            // Dialog GoldType, case sign in
            if (msg.isEmpty()) {
                msg = String.format(activity.getString(R.string.str_welcome), strName);
            }
            android.app.AlertDialog alert = DialogUtils.alertDialogOneButton(activity, activity.getString(R.string.dialog_sign_in_title_success), msg,
                    activity.getString(R.string.confirm_ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
            alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            if (closeCallback != null){
                                closeCallback.onClose();
                            }
                        }
                    });
            alert.show();
        }
        UIApplication.getAppTracker().trackHandling(TrackConstants.HANDLING_SHOW_WELCOME, TrackConstants.CATEGORY_HANDLING_UI, msg);
    }

    public static void showErrorMsg(BaseActivity activity, String msg) {
        if (msg == null) {
            msg = "";
        }
        String strTitle = activity.getString(R.string.str_error);

        if (activity != null) {
            activity.showMessage(strTitle, msg, R.drawable.ic_alert2x);
        }
    }

    */
}
