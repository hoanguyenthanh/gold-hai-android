package com.goldshop.gsh.utils.layouts;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Created by HOA on 07/03/2018.
 */

@SuppressLint("AppCompatCustomView")
public class AutoCompleteExtend extends AutoCompleteTextView {
    public AutoCompleteExtend(Context context) {
        super(context);
    }

    public AutoCompleteExtend(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoCompleteExtend(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AutoCompleteExtend(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @TargetApi(Build.VERSION_CODES.N)
    public AutoCompleteExtend(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, defStyleRes, popupTheme);
    }

    @Override
    protected void performFiltering(CharSequence text, int keyCode) {
        super.performFiltering("", keyCode);
    }

    @Override
    protected void replaceText(CharSequence text) {
        super.replaceText(text);
    }
}
