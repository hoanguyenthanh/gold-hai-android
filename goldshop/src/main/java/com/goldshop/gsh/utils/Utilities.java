package com.goldshop.gsh.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.goldshop.gsh.BuildConfig;
import com.goldshop.gsh.R;
import com.goldshop.gsh.models.OrderDetail;

import java.text.DecimalFormat;
import java.util.List;

import okhttp3.ResponseBody;
import okio.Buffer;

public class Utilities {
    public static DecimalFormat numberFormat = new DecimalFormat("#");
    private static DecimalFormat moneyFormat = new DecimalFormat("###,###,###");

    private static final String V9900 = "V9900";
    private static final String V9950 = "V9950";
    private static final String V9939 = "V9939";
    private static final String V9999 = "V9999";

    private static final String V9850 = "V9850";
    private static final String V979 = "V979";

    private static final String V710 = "V710";

    private static final String V610 = "V610";
    private static final String V585 = "V585";
    private static final String V600 = "V600";
    private static final String V590 = "V590";

//    public static String getGoldCode(String code){
//        if (code.equals(V9900) || code.equals(V9950) || code.equals(V9939) || code.equals(V9999))
//            return V9999;
//
//        if (code.equals(V9850) || code.equals(V979))
//            return V9850;
//
//        if (code.equals(V600) || code.equals(V590) || code.equals(V610) || code.equals(V585))
//            return V610;
//
//        return code;
//    }

    /**
     * Check price is difference
     * @return true if price difference
     */
    public static boolean checkDiffPrice(List<OrderDetail> listOrderDetail){
        if (listOrderDetail != null && listOrderDetail.size() > 0){
            double priceTmp = 0;
            int loop = 0;
            for(OrderDetail dto : listOrderDetail){
                if (loop > 0) {
                    if (dto.getPriceDT() != priceTmp){
                        return true;
                    }
                }
                priceTmp = dto.getPriceDT();
                loop++;
            }
        }

        return false;
    }

    public static String moneyFormat(double value){
        StringBuilder builder = new StringBuilder();
        if (value == 0){
            builder.append("0");
        } else {
            builder.append(moneyFormat.format(value).replace(".", ","));
        }
        return builder.toString();
    }

    public static String trimZeroRight(double value){
        if (value == 0){
            return "";
        }
        String temp = numberFormat.format(value);
        return trimZeroRight(temp);
    }

    private static String trimZeroRight(String value){
        if (value == null){
            return "";
        }

        int index = value.lastIndexOf(".");
        if (index == -1){
            return value;
        }
        return value.substring(0, index);
    }

    public static String replaceStringNull(String value){
        if (value == null){
            return "";
        }
        return value;
    }

    public static String zeroReplaceHyphen(double value){
        if (value == 0){
            return Constants.HYPHEN;
        }
        return moneyFormat(value);
    }

    public static double convertFromString(String str) {
        if (str.contains(",")) {
            str = str.replace(",", "");
        }
        return Double.parseDouble(str);
    }


    public static String getString(String source, int pos){
        return source.substring(pos, 1);
    }

    public static void hideKeySoft(View view, Context context){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    public static ResponseBody cloneResponseBody(ResponseBody responseBody) {
        try {
            final Buffer bufferClone = responseBody.source().buffer().clone();
            return ResponseBody.create(responseBody.contentType(), responseBody.contentLength(), bufferClone);
        } catch (Exception e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static String getStatusName(Context context, String status){

        String name = "";

        if (context == null){
            return name;
        }

        switch (status){
            case Constants.STATUS_NEW:
                name = context.getString(R.string.str_status_new);
                break;
            case Constants.STATUS_CANCELED:
                name = context.getString(R.string.str_status_canceled);
                break;
            case Constants.STATUS_PAID:
                name = context.getString(R.string.str_status_paid);
                break;
            case Constants.STATUS_PRINTED:
                name = context.getString(R.string.str_status_printed);
                break;

        }

        return name;
    }
}
