package com.goldshop.gsh.utils.layouts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.GoldUtils;

/**
 * Created by HOA on 15/06/2017.
 */

public class WeightGoldView extends LinearLayout {
    private View mView;
    private HollerView mHollerView;

    private int mWeight;
    private int mLuong;
    private int mChi;
    private int mPhan;
    private int mLi;
    private GoldUtils goldUtils;

    public void setHyphen(){
        mHollerView.txtValue_L.setText("");
        mHollerView.txtValue_C.setText("");
        mHollerView.txtValue_P.setText("");
        mHollerView.txtValue_Li.setText(Constants.HYPHEN);

        mHollerView.txtSymbol_L.setText("");
        mHollerView.txtSymbol_C.setText("");
        mHollerView.txtSymbol_P.setText("");
        mHollerView.txtSymbol_Li.setText("");
    }

    public void setText(double weight){
        setText((int)weight);
    }

    public void setText(int weight){
        this.mWeight = weight;
        this.goldUtils = GoldUtils.getInstance(this.mWeight);

        this.mLuong = goldUtils.getLuongInt();
        this.mChi = goldUtils.getChiInt();
        this.mPhan = goldUtils.getPhanInt();
        this.mLi = goldUtils.getLiInt();

        setValue();
    }

    public WeightGoldView(Context context) {
        super(context);
        getView(context, null, 0);
    }

    public WeightGoldView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getView(context, attrs, 0);
    }

    public WeightGoldView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getView(context, attrs, defStyleAttr);
    }

    private void getView(Context context, AttributeSet attributeSet, int resId){
        LayoutInflater inflater = LayoutInflater.from(context);
        mView = inflater.inflate(R.layout.weight_gold_view, this, true);
        mHollerView = new HollerView();
        mHollerView.txtValue_L = (TextView) mView.findViewById(R.id.weight_gold_view_value_L);
        mHollerView.txtValue_C = (TextView) mView.findViewById(R.id.weight_gold_view_value_C);
        mHollerView.txtValue_P = (TextView) mView.findViewById(R.id.weight_gold_view_value_P);
        mHollerView.txtValue_Li = (TextView) mView.findViewById(R.id.weight_gold_view_value_Li);

        mHollerView.txtSymbol_L = (TextView) mView.findViewById(R.id.weight_gold_view_symbol_L);
        mHollerView.txtSymbol_C = (TextView) mView.findViewById(R.id.weight_gold_view_symbol_C);
        mHollerView.txtSymbol_P = (TextView) mView.findViewById(R.id.weight_gold_view_symbol_P);
        mHollerView.txtSymbol_Li = (TextView) mView.findViewById(R.id.weight_gold_view_symbol_Li);
    }

    private void setValue(){
        //Luong
        if (this.mLuong == 0){
            mHollerView.txtValue_L.setText("");
            mHollerView.txtSymbol_L.setText("");
        } else {
            mHollerView.txtValue_L.setText(String.valueOf(mLuong));
            mHollerView.txtSymbol_L.setText(Constants.WEIGHT_SYMBOL_LUONG);
        }

        //Chi
        if (this.mChi > 0){
            mHollerView.txtValue_C.setText(String.valueOf(mChi));
            mHollerView.txtSymbol_C.setText(Constants.WEIGHT_SYMBOL_CHI);
        }
        else if (this.mLuong > 0){
            mHollerView.txtValue_C.setText("0");
            mHollerView.txtSymbol_C.setText("");
        }
        else {
            mHollerView.txtValue_C.setText("");
            mHollerView.txtSymbol_C.setText("");
        }

        //Phan
        if (this.mPhan > 0){
            mHollerView.txtValue_P.setText(String.valueOf(mPhan));
            mHollerView.txtSymbol_P.setText(Constants.WEIGHT_SYMBOL_PHAN);
        }
        else if (this.mLuong > 0 || this.mChi > 0){
            mHollerView.txtValue_P.setText("0");
            mHollerView.txtSymbol_P.setText("");
        }
        else {
            mHollerView.txtValue_P.setText("");
            mHollerView.txtSymbol_P.setText("");
        }

        //Li
        if (this.mLi > 0){
            mHollerView.txtValue_Li.setText(String.valueOf(mLi));
            mHollerView.txtSymbol_Li.setText(Constants.WEIGHT_SYMBOL_LI);
        }
        else if (this.mLuong > 0 || this.mChi > 0 || this.mPhan > 0){
            mHollerView.txtValue_Li.setText("0");
            mHollerView.txtSymbol_Li.setText("");
        }
        else {
            mHollerView.txtValue_Li.setText("");
            mHollerView.txtSymbol_Li.setText("");
        }
    }

    private class HollerView{
        TextView txtValue_L;
        TextView txtSymbol_L;
        TextView txtValue_C;
        TextView txtSymbol_C;
        TextView txtValue_P;
        TextView txtSymbol_P;
        TextView txtValue_Li;
        TextView txtSymbol_Li;
    }
}
