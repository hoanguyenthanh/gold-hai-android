package com.goldshop.gsh.utils;

/**
 * Created by HOA on 11/06/2017.
 */

public class GoldUtils {
    private String weightDesc;
    private int mWeight;
    private int mLuong;
    private int mChi;
    private int mPhan;
    private int mLi;
    private String mLuongSym;
    private String mChiSym;
    private String mPhanSym;
    private String mLiSym;

    public static GoldUtils getInstance(double weight){
        GoldUtils gold = new GoldUtils(weight);
        return gold;
    }

    public GoldUtils(double weight){
        mWeight = 0;
        if (weight > 0){
            String strTrim = Utilities.trimZeroRight(weight);
            mWeight = Integer.valueOf(strTrim);
        }
        convert();
    }

    public GoldUtils(int weight){
        mWeight = weight;
        convert();
    }

    public String getWeightDesc(){
        return this.weightDesc;
    }

    public String getLuongStr(){
        if (mLuong == 0){
            return "";
        } else {
            return String.valueOf(mLuong);
        }
    }

    public int getLuongInt(){
        return mLuong;
    }

    public String getLuongSymbol(){
        return this.mLuongSym;
    }

    public String getChiStr(){
        if (mChi == 0){
            return "";
        } else {
            return String.valueOf(mChi);
        }
    }

    public int getChiInt(){
        return this.mChi;
    }

    public String getChiSymbol(){
        return this.mChiSym;
    }

    public String getPhanStr(){
        if (mPhan == 0){
            return "";
        } else {
            return String.valueOf(mPhan);
        }
    }

    public String getPhanSymbol(){
        return this.mPhanSym;
    }

    public int getPhanInt(){
        return mPhan;
    }

    public String getLiStr(){
        if (mLi == 0){
            return "";
        } else {
            return String.valueOf(mLi);
        }
    }

    public int getLiInt(){
        return mLi;
    }

    public String getLiSymbol(){
        return this.mLiSym;
    }

    private void convert(){
        mLi = getLi();
        mPhan = getPhan();
        mChi = getChi();
        mLuong = getLuong();
        weightDesc = weightValue2Desc();

        mLuongSym = mLuong > 0 ? Constants.WEIGHT_SYMBOL_LUONG : "";
        mChiSym = mChi > 0 ? Constants.WEIGHT_SYMBOL_CHI : "";
        mPhanSym = mPhan > 0 ? Constants.WEIGHT_SYMBOL_PHAN : "";
        mLiSym = mLi > 0 ? Constants.WEIGHT_SYMBOL_LI : "";
    }

    public String weightValue2Desc(){
        String strLi = mLi > 0 ? mLi + "Li" : "0";
        String strPhan = mPhan > 0 ? mPhan + "p" : "0";
        String strChi = mChi > 0 ? mChi + "c" : "0";
        String strLuong = mLuong > 0 ? mLuong + "L" : "0";

        StringBuilder result = new StringBuilder();
        if (mLuong > 0){
            result.append(strLuong);
            result.append(strChi);
            result.append(strPhan);
            result.append(strLi);
        }
        else if (mLuong == 0 && mChi > 0){
            result.append(strChi);
            result.append(strPhan);
            result.append(strLi);
        }
        else if (mLuong == 0 && mChi == 0 && mPhan > 0){
            result.append(strPhan);
            result.append(strLi);
        }
        else if (mLuong == 0 && mChi == 0 && mPhan == 0 && mLi > 0){
            result.append(strLi);
        }
        else {
            result.append(Constants.HYPHEN);
        }
        return result.toString();
    }

    private int getLuong(){
        int luong = 0;
        if (mWeight >= 1000){
            luong = mWeight / 1000;
        }
        return luong;
    }

    private int getChi(){
        int chi = 0;
        if (mWeight >= 100){
            int tmp = mWeight % 1000;
            chi = tmp/100;
        }
        return chi;
    }

    private int getPhan(){
        int phan = 0;
        if (mWeight >= 10){
            int tmp = mWeight % 100;
            phan = tmp/10;
        }
        return phan;
    }

    private int getLi(){
        int li = 0;
        if (mWeight > 0){
            li = mWeight % 10;
        }
        return li;
    }
}
