package com.goldshop.gsh.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.goldshop.gsh.models.User;
import com.google.gson.Gson;


public class Setting {
    private static final String APP_SETTING = "COM_HTN_GOLD_APP_SETTING";

    private static final String SERVER_PORT = "KEY_SERVER_PORT";
    private static final String SERVER_NAME = "KEY_SERVER_NAME";
    private static final String LOGIN_ACCOUNT = "KEY_LOGIN_ACCOUNT";
    private static final String LOGIN_PASS = "KEY_LOGIN_PASS";
    private static final String LOGIN_SWITCH_ON_OFF = "KEY_LOGIN_SWITCH_ON_OFF";
    private static final String LOGIN_USER_INTO = "KEY_LOGIN_USER_INFO";

    private static final String GOLD_PRICE_SELL = "GOLD_PRICE_SELL";
    private static final String GOLD_PRICE_BUY = "GOLD_PRICE_BUY";

    private static final String IS_GET_DATA_OLD = "KEY_IS_GET_DATA_OLD";

    private Context mContext;
    private SharedPreferences mSharedPreferences;


    public Setting(Context context){
        mContext = context;
        mSharedPreferences = mContext.getSharedPreferences(APP_SETTING, mContext.MODE_PRIVATE);
    }


    public void putIP(String serverName){
        mSharedPreferences.edit().putString(SERVER_NAME, serverName).commit();
    }

    public String getIP(){
        return mSharedPreferences.getString(SERVER_NAME, "192.168.1.217");
    }

    //Is get data old invoice
    public int isSearchDataOld(){
        return mSharedPreferences.getInt(IS_GET_DATA_OLD, 0);
    }

    public void putSearchDataOld(int isGetDataOld){
        mSharedPreferences.edit()
                .putInt(IS_GET_DATA_OLD, isGetDataOld)
                .commit();
    }

    //Port
    public void putPort(int port){
        mSharedPreferences.edit()
                .putInt(SERVER_PORT, port)
                .commit();
    }

    public int getServerPort(){
        return mSharedPreferences.getInt(SERVER_PORT, 8080);
    }

    //Login account
    public void putUserName(String account){
        mSharedPreferences.edit()
                .putString(LOGIN_ACCOUNT, account)
                .commit();
    }

    public String getLoginAccount(){
        return mSharedPreferences.getString(LOGIN_ACCOUNT, "");
    }

    //Login password
    public void putPassword(String password){
        mSharedPreferences.edit()
                .putString(LOGIN_PASS, password)
                .commit();
    }

    public String getPassword(){
        return mSharedPreferences.getString(LOGIN_PASS, "");
    }


    public void putUserInfo(User user){
        if (user != null) {
            String toJson = new Gson().toJson(user);
            mSharedPreferences.edit()
                    .putString(LOGIN_USER_INTO, toJson)
                    .commit();
        }
    }

    public User getUserInfo(){
        String strUser = mSharedPreferences.getString(LOGIN_USER_INTO, null);
        if (strUser != null){
            return new Gson().fromJson(strUser, User.class);
        } else {
            return null;
        }
    }

    //Login switch on/off
    public void putIsSavePassword(boolean onOff){
        mSharedPreferences.edit()
                .putBoolean(LOGIN_SWITCH_ON_OFF, onOff)
                .commit();
    }

    public boolean isSavePassword(){
        return mSharedPreferences.getBoolean(LOGIN_SWITCH_ON_OFF, false);
    }
}
