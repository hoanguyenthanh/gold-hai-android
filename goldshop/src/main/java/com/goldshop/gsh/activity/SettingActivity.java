package com.goldshop.gsh.activity;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import com.goldshop.gsh.R;
import com.goldshop.gsh.utils.Setting;


public class SettingActivity extends AppCompatActivity {

    private EditText editServerName, editPort;
    private EditText editPriceSell, editPriceBuy;
    private Setting mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.setting_activiy);

        mSettings = new Setting(this);

        editServerName = (EditText) findViewById(R.id.setting_activity_edit_server);
        editPort = (EditText) findViewById(R.id.setting_activity_edit_port);
        editPriceSell = (EditText) findViewById(R.id.setting_activity_edit_price_sell);
        editPriceBuy = (EditText) findViewById(R.id.setting_activity_edit_price_buy);

        editServerName.setText(mSettings.getIP());
        editPort.setText(String.valueOf(mSettings.getServerPort()));

        findViewById(R.id.btn_finish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValid()){
                    return;
                }

                mSettings.putIP(editServerName.getText().toString());
                mSettings.putPort(Integer.parseInt(editPort.getText().toString()));

                SettingActivity.this.finish();
            }
        });
    }

    private boolean isValid(){

        if (TextUtils.isEmpty(editServerName.getText().toString())
                || TextUtils.isEmpty(editPort.getText().toString())){
            AlertDialog alertDialog = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Cảnh báo")
                    .setMessage("IP máy chủ và Port không được để trống!")
                    .setNeutralButton("Đóng", (dialogInterface, i) -> {
                        if (dialogInterface != null){
                            dialogInterface.dismiss();
                        }
                    });
            alertDialog = builder.create();
            alertDialog.show();
            return false;
        }

        return true;
    }
}
