package com.goldshop.gsh.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.invoice.InvoiceActivity;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.Setting;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtLogout;
    private Setting mSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_container);

        mSetting = new Setting(this);

        txtLogout = findViewById(R.id.main_container_txtLogout);
        txtLogout.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        setListener();
    }

    public void setListener(){
        findViewById(R.id.main_container_btn_sell).setOnClickListener(this);
        findViewById(R.id.main_container_btn_buy).setOnClickListener(this);
        findViewById(R.id.main_container_btn_sell_and_buy).setOnClickListener(this);
        findViewById(R.id.main_container_btn_list_invoice).setOnClickListener(this);
        txtLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        Intent intent = new Intent(this, InvoiceActivity.class);
        intent.putExtra(Constants.EXTRA_ORDER_ID, 0);

        if (id == R.id.main_container_btn_sell){
            intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.SELL);
        }
        else if (id == R.id.main_container_btn_buy){
            intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.BUY);
        }
        else if (id == R.id.main_container_btn_sell_and_buy){
            intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.SELL_BUY);
        }
        else if (id == R.id.main_container_btn_list_invoice){
            intent = new Intent(this, ListInvoiceActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        } else {
            intent = null;
        }

        if (intent != null){
            startActivity(intent);
        }
        else if (id == txtLogout.getId()){
            mSetting.putPassword("");

            intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
