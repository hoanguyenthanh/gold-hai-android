package com.goldshop.gsh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.goldshop.gsh.R;
import com.goldshop.gsh.utils.Constants;

import me.dm7.barcodescanner.zbar.ZBarScannerView;


public class ZBarScannerActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler, View.OnClickListener {
    private static final String FLASH_STATE = "FLASH_STATE";

    private ZBarScannerView mScannerView;
    private boolean mFlash;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.invoice_barcode_scanner_activity);
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.camera_container);

        mScannerView = new ZBarScannerView(this);
        contentFrame.addView(mScannerView);

        findViewById(R.id.btn_close).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
    }

    @Override
    public void handleResult(me.dm7.barcodescanner.zbar.Result rawResult) {
        if (rawResult != null){
            mScannerView.stopCamera();
            Intent resultIntent = new Intent();
            resultIntent.putExtra(Constants.EXTRA_CODE_PRODUCT, rawResult.getContents());
            resultIntent.putExtra(Constants.EXTRA_CODE_FORMAT, rawResult.getBarcodeFormat().getName());
            setResult(RESULT_OK, resultIntent);
            finish();
        }
    }

    public void toggleFlash(View v) {
        mFlash = !mFlash;
        mScannerView.setFlash(mFlash);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_close){
            mScannerView.stopCamera();
            setResult(RESULT_CANCELED, new Intent());
            finish();
        }
    }
}