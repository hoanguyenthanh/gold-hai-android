package com.goldshop.gsh.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.adapter.GoldTypeAdapter;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.models.GoldType;
import com.goldshop.gsh.utils.RecyclerViewItemClickSupport;
import com.goldshop.gsh.utils.Setting;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GoldTypeDialogFragment extends DialogFragment {
    public static final String TAG = GoldTypeDialogFragment.class.getSimpleName();
    public static final String KEY_GOLD_TYPE = "key_gold_type";


    private OrderAPI mOrderAPI;
    private Setting mSetting;

    private RecyclerView.LayoutManager mLayoutManager;
    private Context mContext;
    private GoldTypeAdapter goldTypeAdapter;
    private RecyclerView rvRecyclerView;
    private TextView tvClose;
    private ProgressBar progressBar;

    private List<GoldType> goldTypeList;
    private String curGoldType;

    private OnGoldTypeSelect onGoldTypeSelect;

    public interface OnGoldTypeSelect {
        void onSelect(GoldType goldType);
    }


    public static GoldTypeDialogFragment newInstance(String goldType, OnGoldTypeSelect callback) {
        GoldTypeDialogFragment fragment = new GoldTypeDialogFragment();

        fragment.onGoldTypeSelect = callback;
        fragment.curGoldType = goldType;

        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mSetting = new Setting(getContext());
        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null){
                    getActivity().getSupportFragmentManager()
                            .popBackStack();
                }
            }
        });

        RecyclerViewItemClickSupport.addTo(rvRecyclerView).setOnItemClickListener(new RecyclerViewItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                GoldType goldType = goldTypeList.get(position);
                if (onGoldTypeSelect != null){
                    onGoldTypeSelect.onSelect(goldType);
                }

                if (getActivity() != null){
                    getActivity().getSupportFragmentManager()
                            .popBackStack();
                }
            }
        });


        getGoldTypes();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gold_type_dialog, container, false);

        rvRecyclerView = view.findViewById(R.id.rvRecyclerView);
        tvClose = view.findViewById(R.id.tv_close);
        progressBar = view.findViewById(R.id.pbProgressBar);

        progressBar.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(getContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);

        return view;
    }

    private void showLoading(){
        if (progressBar != null){
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideLoading(){
        if (progressBar != null){
            progressBar.setVisibility(View.GONE);
        }
    }

    private void getGoldTypes(){
        showLoading();

        mOrderAPI.getGoldType().enqueue(new Callback<List<GoldType>>() {
            @Override
            public void onResponse(Call<List<GoldType>> call, Response<List<GoldType>> response) {

                if (isAdded()) {
                    hideLoading();

                    if (response.isSuccessful()) {
                        goldTypeList = response.body();

                        if (goldTypeAdapter == null){
                            goldTypeAdapter = new GoldTypeAdapter(goldTypeList, curGoldType);
                            rvRecyclerView.setAdapter(goldTypeAdapter);
                        } else {
                            goldTypeAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<GoldType>> call, Throwable t) {
                if (isAdded()) {
                    Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    hideLoading();
                }
            }
        });
    }
}
