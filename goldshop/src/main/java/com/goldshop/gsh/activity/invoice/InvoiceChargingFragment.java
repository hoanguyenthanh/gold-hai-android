package com.goldshop.gsh.activity.invoice;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.BaseFragment;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.CommonResponse;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.GoldUtils;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.Utilities;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceChargingFragment  extends BaseFragment implements View.OnClickListener{
    public static final String TAG = InvoiceChargingFragment.class.getSimpleName();

    private OrderAPI mOrderAPI;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    private Call<CommonResponse> callUpdateInfoPrint;
    private List<OrderDetail> listOrderSell = new ArrayList<>();
    private List<OrderDetail> listOrderBuy = new ArrayList<>();
    private List<OrderDetail> listOrderDetail = new ArrayList<>();

    private LinearLayout mProgressBar;
    private RelativeLayout relativeBackInvoice;
    private OnPagingListener onPagingListener;
    private HollerText mHollerText;

    private Setting mSetting;

    private long mOrderId;
    private String mTypeTransaction = "";


    public static InvoiceChargingFragment newInstance() {
        InvoiceChargingFragment fragment = new InvoiceChargingFragment();
        return fragment;
    }


    public void setOrderId(long _orderId){
        mOrderId = _orderId;
    }

    public void setTypeTransaction(String _typeTransaction) {
        this.mTypeTransaction = _typeTransaction;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSetting = new Setting(getContext());
        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
        mHollerText = new HollerText();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.invoice_charging_fragment, null, false);

        mProgressBar = mView.findViewById(R.id.invoice_charging_linear_process_bar);

        mHollerText.btnPrinter = mView.findViewById(R.id.invoice_charging_btn_print);
        mHollerText.btnPrinter.setOnClickListener(this);

        mHollerText.btnVerify = mView.findViewById(R.id.invoice_charging_btn_verify);
        mHollerText.btnVerify.setOnClickListener(this);

        relativeBackInvoice = mView.findViewById(R.id.invoice_charging_btn_back);
        relativeBackInvoice.setOnClickListener(this);

        mHollerText.linear_charging_total_weight_sell = mView.findViewById(R.id.linear_charging_total_weight_sell);
        mHollerText.linear_charging_total_weight_buy = mView.findViewById(R.id.linear_charging_total_weight_buy);
        mHollerText.linear_charging_price_sell = mView.findViewById(R.id.linear_charging_price_sell);
        mHollerText.linear_charging_price_buy = mView.findViewById(R.id.linear_charging_price_buy);
        mHollerText.linear_charging_total_salary = mView.findViewById(R.id.linear_charging_total_salary);
        mHollerText.linear_charging_total_pay_bonus = mView.findViewById(R.id.linear_charging_total_pay_bonus);

        mHollerText.txtWeightSell = mView.findViewById(R.id.txt_charging_total_weight_sell);
        mHollerText.txtWeightBuy = mView.findViewById(R.id.txt_charging_total_weight_buy);
        mHollerText.txtTotalWeight = mView.findViewById(R.id.txt_charging_total_weight);
        mHollerText.txtPriceSell = mView.findViewById(R.id.txt_charging_price_sell);
        mHollerText.txtPriceBuy = mView.findViewById(R.id.txt_charging_price_buy);
        mHollerText.txtAmount = mView.findViewById(R.id.txt_charging_amount);
        mHollerText.txtSalary = mView.findViewById(R.id.txt_charging_total_salary);
        mHollerText.txtPayBonus = mView.findViewById(R.id.txt_charging_total_pay_bonus);
        mHollerText.txtTotalPay = mView.findViewById(R.id.txt_charging_total_pay);

        mHollerText.lbl_charging_total_weight_buy = mView.findViewById(R.id.lbl_charging_total_weight_buy);

        mHollerText.lblTotalWeight = mView.findViewById(R.id.txt_charging_label_total_weight);
        mHollerText.lblTotalPay = mView.findViewById(R.id.txt_charging_label_total_pay);

        mHollerText.txtWeightSell.setHyphen();
        mHollerText.txtWeightBuy.setHyphen();

        mHollerText.txtPriceSell.setText(Constants.HYPHEN);
        mHollerText.txtPriceBuy.setText(Constants.HYPHEN);
        mHollerText.txtAmount.setText(Constants.HYPHEN);
        mHollerText.txtSalary.setText(Constants.HYPHEN);
        mHollerText.txtPayBonus.setText(Constants.HYPHEN);
        mHollerText.txtTotalWeight.setHyphen();
        mHollerText.txtTotalPay.setText(Constants.HYPHEN);


        setVisibleDeleteButton(false);

        return mView;
    }

    private void setVisibleDeleteButton(boolean visible){
        if (getActivity() != null && getActivity() instanceof InvoiceActivity){
            ((InvoiceActivity) getActivity()).setVisibleDeleteButton(visible);
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            loadOrderDetail();
        }
    }

    private void showLoading(boolean show){
        if (mProgressBar != null){
            if (show){
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                mProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == relativeBackInvoice.getId()){
            onPagingListener.setPageIndex(InvoiceActivity.PAGE_INDEX_PRODUCTS);
        }
        else if (id == mHollerText.btnPrinter.getId()){
            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }

            if (mOrderId > 0){
                sendPrinter(mOrderId);
            }
        }
        else if (id == mHollerText.btnVerify.getId()){
            updateStatusOrder(Constants.STATUS_PAID);
        }
    }


    private StringBuilder lblWeightBuy = new StringBuilder();
    private StringBuilder lblTotalWeight = new StringBuilder();
    private StringBuilder lblTotalPay = new StringBuilder();
    private GoldUtils sellTotalWeightUtils;
    private GoldUtils buyTotalWeightUtils;
    private GoldUtils totalWeightUtils;

    private void charging(){
        if (listOrderSell.size() > 0 || listOrderBuy.size() > 0) {
            double priceSell = 0;
            double priceBuy = 0;
            int sellTotalWeight = 0;
            int buyTotalWeight = 0;

            double amountSell = 0;
            double amountBuy = 0;
            double amountSell_IncludeSalary = 0;
            double amountBuy_IncludeBonusPay = 0;
            double totalSalary = 0;
            double totalPayBonus = 0;

            for(OrderDetail sell : listOrderSell){
                sellTotalWeight += sell.getTotalWeightDT();
                totalSalary += sell.getSalary();
                priceSell = sell.getPriceDT();
                amountSell += (sell.getTotalWeightDT()/100.0) * sell.getPriceDT();
                amountSell_IncludeSalary += sell.getAmountDT(); //Include salary
            }
            for(OrderDetail buy : listOrderBuy){
                buyTotalWeight += buy.getTotalWeightDT();
                totalPayBonus += buy.getPayBonusAmount();
                priceBuy = buy.getPriceDT();
                amountBuy += (buy.getTotalWeightDT()/100.0) * buy.getPriceDT();
                amountBuy_IncludeBonusPay += buy.getAmountDT(); //Include bonus pay
            }

            if (sellTotalWeight == 0){
                priceSell = 0;
            }
            if (buyTotalWeight == 0){
                priceBuy = 0;
            }

            double totalPay = 0;
            double amount = 0;
            int totalWeight = sellTotalWeight - buyTotalWeight;

            int priceSellVisible = View.VISIBLE;
            int priceBuyVisible = View.VISIBLE;

            lblWeightBuy.setLength(0);
            lblTotalWeight.setLength(0);
            lblTotalPay.setLength(0);
            if (Constants.SELL.equals(mTypeTransaction)) { // Sell
                amount = 1000 * amountSell;
                totalPay = 1000 * amountSell_IncludeSalary;
                priceBuy = 0;

                priceSellVisible = View.VISIBLE;
                priceBuyVisible = View.GONE;
                if (Utilities.checkDiffPrice(listOrderSell)) {
                    priceSell = 0;
                    priceSellVisible = View.GONE;
                }
                lblWeightBuy.append(getString(R.string.str_total_gold_buy));
                lblTotalWeight.append(getString(R.string.str_total_gold_custom_pay));
                lblTotalPay.append(getString(R.string.str_total_money_custom_pay));
            }
            else if (Constants.BUY.equals(mTypeTransaction)) { //Buy
                amount = 1000 * amountBuy;
                totalPay = 1000 * amountBuy_IncludeBonusPay;
                priceSell = 0;

                priceSellVisible = View.GONE;
                priceBuyVisible = View.VISIBLE;
                if (Utilities.checkDiffPrice(listOrderBuy)) {
                    priceBuy = 0;
                    priceBuyVisible = View.GONE;
                }
                lblWeightBuy.append(getString(R.string.str_total_gold_buy));
                lblTotalWeight.append(getString(R.string.str_total_gold_store_pay));
                lblTotalPay.append(getString(R.string.str_total_money_store_pay));
            }
            else if (Constants.SELL_BUY.equals(mTypeTransaction) && totalWeight > 0) { //Sell and buy
                if (Utilities.checkDiffPrice(listOrderSell)){
                    showAlertDialog(getString(R.string.str_msg_005), getString(R.string.str_msg_007));
                    onPagingListener.setPageIndex(InvoiceActivity.PAGE_INDEX_PRODUCTS);
                    return;
                }
                amount = 1000 * ((totalWeight/100.0) * priceSell);
                totalPay = amount + (totalSalary + totalPayBonus) * 1000;
                priceBuy = 0;

                priceSellVisible = View.VISIBLE;
                priceBuyVisible = View.GONE;
                lblWeightBuy.append(getString(R.string.str_total_gold_exchange));
                lblTotalWeight.append(getString(R.string.str_total_gold_custom_pay));
                lblTotalPay.append(getString(R.string.str_total_money_custom_pay));
            }
            else if (Constants.SELL_BUY.equals(mTypeTransaction) && totalWeight < 0) { //Sell and buy
                if (Utilities.checkDiffPrice(listOrderBuy)){
                    showAlertDialog(getString(R.string.str_msg_005), getString(R.string.str_msg_006));
                    onPagingListener.setPageIndex(InvoiceActivity.PAGE_INDEX_PRODUCTS);
                    return;
                }
                priceSell = 0;
                amount = 1000 * ((totalWeight/100.0) * priceBuy); // Se la so am
                totalPay = amount + (totalSalary + totalPayBonus) * 1000; // se la so am

                priceSellVisible = View.GONE;
                priceBuyVisible = View.VISIBLE;
                lblWeightBuy.append(getString(R.string.str_total_gold_exchange));
                lblTotalWeight.append(getString(R.string.str_total_gold_store_pay));
                lblTotalPay.append(getString(R.string.str_total_money_store_pay));
            }
            else if (Constants.SELL_BUY.equals(mTypeTransaction)&& totalWeight == 0){
                priceSell = 0;
                priceBuy = 0;
                amount = 0;
                totalPay = (totalSalary + totalPayBonus) * 1000;

                priceSellVisible = View.GONE;
                priceBuyVisible = View.GONE;
                lblWeightBuy.append(getString(R.string.str_total_gold_exchange));
                lblTotalWeight.append(getString(R.string.str_total_gold_custom_pay));
                lblTotalPay.append(getString(R.string.str_total_money_custom_pay));
            }

            totalSalary = 1000 * totalSalary;
            totalPayBonus = 1000 * totalPayBonus;
            priceSell *= 1000;
            priceBuy *= 1000;

            sellTotalWeightUtils = GoldUtils.getInstance(sellTotalWeight);
            buyTotalWeightUtils = GoldUtils.getInstance(buyTotalWeight);
            totalWeightUtils = GoldUtils.getInstance(Math.abs(totalWeight));

            int sellTotalWeight_L = sellTotalWeightUtils.getLuongInt();
            int sellTotalWeight_C = sellTotalWeightUtils.getChiInt();
            int sellTotalWeight_P = sellTotalWeightUtils.getPhanInt();
            int sellTotalWeight_Li = sellTotalWeightUtils.getLiInt();

            int buyTotalWeight_L = buyTotalWeightUtils.getLuongInt();
            int buyTotalWeight_C = buyTotalWeightUtils.getChiInt();
            int buyTotalWeight_P = buyTotalWeightUtils.getPhanInt();
            int buyTotalWeight_Li = buyTotalWeightUtils.getLiInt();

            int totalWeight_L = totalWeightUtils.getLuongInt();
            int totalWeight_C = totalWeightUtils.getChiInt();
            int totalWeight_P = totalWeightUtils.getPhanInt();
            int totalWeight_Li = totalWeightUtils.getLiInt();

            if (sellTotalWeight == 0) {
                mHollerText.txtWeightSell.setHyphen();
            } else {
                mHollerText.txtWeightSell.setText(sellTotalWeight);
            }
            if (buyTotalWeight == 0) {
                mHollerText.txtWeightBuy.setHyphen();
            } else {
                mHollerText.txtWeightBuy.setText(buyTotalWeight);
            }

            mHollerText.txtPriceSell.setText(Utilities.zeroReplaceHyphen(priceSell));
            mHollerText.txtPriceBuy.setText(Utilities.zeroReplaceHyphen(priceBuy));
            mHollerText.txtAmount.setText(Utilities.zeroReplaceHyphen(Math.abs(amount)));
            mHollerText.txtSalary.setText(Utilities.zeroReplaceHyphen(totalSalary));
            mHollerText.txtPayBonus.setText(Utilities.zeroReplaceHyphen(totalPayBonus));

            mHollerText.lblTotalWeight.setText(lblTotalWeight.toString());
            if (totalWeight == 0) {
                mHollerText.txtTotalWeight.setHyphen();
            } else {
                mHollerText.txtTotalWeight.setText(Math.abs(totalWeight));
            }

            mHollerText.lblTotalPay.setText(lblTotalPay.toString());
            mHollerText.txtTotalPay.setText(Utilities.zeroReplaceHyphen(Math.abs(totalPay)));

            setVisibleItem(mTypeTransaction);
            mHollerText.linear_charging_price_sell.setVisibility(priceSellVisible);
            mHollerText.linear_charging_price_buy.setVisibility(priceBuyVisible);
            mHollerText.lbl_charging_total_weight_buy.setText(lblWeightBuy.toString());


            //Update info  print
            callUpdateInfoPrint = mOrderAPI.updateInfoPrint(mOrderId
                    , sellTotalWeight
                    , sellTotalWeight_L
                    , sellTotalWeight_C
                    , sellTotalWeight_P
                    , sellTotalWeight_Li
                    , ""
                    , buyTotalWeight
                    , buyTotalWeight_L
                    , buyTotalWeight_C
                    , buyTotalWeight_P
                    , buyTotalWeight_Li
                    , ""
                    , totalWeight
                    , totalWeight_L
                    , totalWeight_C
                    , totalWeight_P
                    , totalWeight_Li
                    , ""
                    , priceSell
                    , priceBuy
                    , amount
                    , totalSalary
                    , totalPayBonus
                    , totalPay);

            getStatusOrder();
        }
    }
    private void getStatusOrder(){
        showLoading(true);
        mOrderAPI.getStatusOrder(mOrderId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isAdded()) {
                    showLoading(false);

                    if (response.isSuccessful()) {
                        CommonResponse commonResponse = response.body();
                        String status = commonResponse.status == null ? "" : commonResponse.status;

                        if (Constants.STATUS_NEW.equals(status) || Constants.STATUS_PRINTED.equals(status)) {
                            updateInfoPrint();
                            mHollerText.btnVerify.setVisibility(View.VISIBLE);
                        } else {
                            mHollerText.btnPrinter.setEnabled(true);
                            mHollerText.btnVerify.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        mHollerText.btnPrinter.setEnabled(true);
                        mHollerText.btnVerify.setVisibility(View.INVISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!isAdded()) return;
                showLoading(false);

                mHollerText.btnPrinter.setEnabled(true);
                mHollerText.btnVerify.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void updateInfoPrint(){
        showLoading(true);

        callUpdateInfoPrint.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isAdded()) {
                    showLoading(false);

                    if (response.isSuccessful() == false || response.body().msgCode <= 0) {
                        Toast.makeText(getActivity(), getString(R.string.str_msg_003), Toast.LENGTH_LONG).show();
                    }
                    mHollerText.btnPrinter.setEnabled(true);
                    mHollerText.btnVerify.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (isAdded()) {
                    showLoading(false);

                    Toast.makeText(getActivity(), getString(R.string.str_msg_003), Toast.LENGTH_LONG).show();
                    mHollerText.btnPrinter.setEnabled(true);
                    mHollerText.btnVerify.setEnabled(true);
                }
            }
        });
    }

    private void setVisibleItem(String typeSellBuy){
        if (typeSellBuy.equals(Constants.SELL)){
            mHollerText.linear_charging_total_weight_sell.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_weight_buy.setVisibility(View.GONE);
            mHollerText.linear_charging_total_salary.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_pay_bonus.setVisibility(View.GONE);
        }
        else if (typeSellBuy.equals(Constants.BUY)){
            mHollerText.linear_charging_total_weight_sell.setVisibility(View.GONE);
            mHollerText.linear_charging_total_weight_buy.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_salary.setVisibility(View.GONE);
            mHollerText.linear_charging_total_pay_bonus.setVisibility(View.VISIBLE);
        }
        else if (typeSellBuy.equals(Constants.SELL_BUY)){
            mHollerText.linear_charging_total_weight_sell.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_weight_buy.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_salary.setVisibility(View.VISIBLE);
            mHollerText.linear_charging_total_pay_bonus.setVisibility(View.VISIBLE);
        }
    }

    private void sendPrinter(long orderId) {
        showLoading(true);

        mHollerText.btnPrinter.setEnabled(false);
        mHollerText.btnVerify.setEnabled(false);

        mOrderAPI.sendPrinter(orderId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isAdded()) {
                    showLoading(false);

                    if (response.isSuccessful()) {
                        if (response.body().msgCode <= 0) {
                            showAlertDialog("Lỗi", response.body().msgString);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.str_send_print_finish), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.str_msg_002), Toast.LENGTH_LONG).show();
                    }
                    mHollerText.btnPrinter.setEnabled(true);
                    mHollerText.btnVerify.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (isAdded()) {
                    showLoading(false);

                    Toast.makeText(getActivity(), getString(R.string.str_msg_002), Toast.LENGTH_LONG).show();
                    mHollerText.btnPrinter.setEnabled(true);
                    mHollerText.btnVerify.setEnabled(true);
                }
            }
        });
    }

    private void updateStatusOrder(String status){
        showLoading(true);
        mHollerText.btnVerify.setEnabled(false);

        mOrderAPI.updateStatusOrder(mOrderId, status).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (isAdded()){
                    mHollerText.btnVerify.setEnabled(true);
                    showLoading(false);

                    if (response.isSuccessful()){
                        if (response.body().msgCode > 0){
                            Toast.makeText(getActivity(), getString(R.string.str_msg_018), Toast.LENGTH_LONG).show();
                            mHandler.postDelayed(() -> {
                                if (getActivity() != null){
                                    getActivity().finish();
                                }
                            }, 1500);
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.str_msg_017), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.str_msg_017), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (isAdded()){
                    mHollerText.btnVerify.setEnabled(true);
                    showLoading(false);
                    Toast.makeText(getActivity(), getString(R.string.str_msg_017), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void loadOrderDetail(){
        if (mOrderId > 0) {
            showLoading(true);

            mHollerText.btnPrinter.setEnabled(false);
            mHollerText.btnVerify.setEnabled(false);

            mOrderAPI.getOrderDetails(mOrderId, 0, "").enqueue(new Callback<List<OrderDetail>>() {
                @Override
                public void onResponse(Call<List<OrderDetail>> call, Response<List<OrderDetail>> response) {
                    if (isAdded()) {
                        showLoading(false);

                        listOrderDetail = response.body();
                        listOrderSell.clear();
                        listOrderBuy.clear();

                        if (listOrderDetail != null && listOrderDetail.size() > 0) {
                            for (OrderDetail dto : listOrderDetail) {
                                if (dto.getTypeTrans().equals(Constants.SELL)) {
                                    listOrderSell.add(dto);
                                } else if (dto.getTypeTrans().equals(Constants.BUY)) {
                                    listOrderBuy.add(dto);
                                }
                            }
                        }

                        mHandler.post(() -> charging());
                    }
                }

                @Override
                public void onFailure(Call<List<OrderDetail>> call, Throwable t) {
                    if (isAdded()) {
                        showLoading(false);

                        mHollerText.btnPrinter.setEnabled(true);
                        mHollerText.btnVerify.setEnabled(true);
                        Toast.makeText(getActivity(), getString(R.string.str_err_can_not_connect_server), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setOnPagingListener(OnPagingListener listener){
        this.onPagingListener = listener;
    }

    public interface OnPagingListener{
        void setPageIndex(int pageIndex);
    }

    private class HollerText{
        LinearLayout linear_charging_total_weight_sell;
        LinearLayout linear_charging_total_weight_buy;
        LinearLayout linear_charging_price_sell;
        LinearLayout linear_charging_price_buy;
        LinearLayout linear_charging_total_salary;
        LinearLayout linear_charging_total_pay_bonus;

        WeightGoldView txtWeightSell;
        WeightGoldView txtWeightBuy;
        WeightGoldView txtTotalWeight;
        TextView txtPriceSell;
        TextView txtPriceBuy;
        TextView txtAmount;
        TextView txtSalary;
        TextView txtPayBonus;
        TextView txtTotalPay;

        TextView lbl_charging_total_weight_buy;
        TextView lblTotalWeight;
        TextView lblTotalPay;

        Button btnPrinter;
        Button btnVerify;
    }
}
