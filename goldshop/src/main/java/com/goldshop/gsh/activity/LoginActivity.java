package com.goldshop.gsh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.models.User;
import com.goldshop.gsh.utils.AlertUtils;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.Utilities;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private View mView;
    private SwitchCompat switchSavePassword;
    private ProgressBar mProgressBar;
    private EditText txtUserID, txtPwd;
    private Setting mSetting;
    private OrderAPI mOrderAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        mSetting = new Setting(this);
        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);


        txtUserID = findViewById(R.id.txt_user_id);
        txtPwd = findViewById(R.id.txt_pwd);
        switchSavePassword = findViewById(R.id.switch_save_pass);
        mProgressBar = findViewById(R.id.login_progress);

        if (mSetting.isSavePassword() == true) {
            String userName = mSetting.getLoginAccount();
            String pwd = mSetting.getPassword();

            txtUserID.setText(userName);
            txtPwd.setText(pwd);
            switchSavePassword.setChecked(true);

            if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pwd)){
                login(userName, pwd);
            }
        }else{
            switchSavePassword.setChecked(false);
        }

        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.img_setting).setOnClickListener(this);

        mView = findViewById(R.id.linear_login);
        mView.setOnClickListener(this);


        txtUserID.setOnClickListener(this);
        txtPwd.setOnClickListener(this);
    }

    private void login(String userName, String pwd){

        if (getWindow().getCurrentFocus() != null)
            hideSoftKeyboard(getWindow().getCurrentFocus());

        if (switchSavePassword.isChecked()) {
            mSetting.putUserName(userName);
            mSetting.putPassword(pwd);
            mSetting.putIsSavePassword(true);
        }else{
            mSetting.putUserName("");
            mSetting.putPassword("");
            mSetting.putIsSavePassword(false);
        }

        mProgressBar.setVisibility(View.VISIBLE);

        mOrderAPI.login(userName, pwd)
            .enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);

                    if (response.isSuccessful()){
                        User user = response.body();

                        if (user != null && userName.equals(user.UserName) && pwd.equals(user.Password)){
                            mSetting.putUserInfo(user);
                            startMainActivity();
                        } else {
                            AlertUtils.getAlertClose(LoginActivity.this, "Lỗi",  "Tên đăng nhập hoặc mật khẩu không đúng", null)
                                    .show();
                        }
                    } else {
                        AlertUtils.getAlertClose(LoginActivity.this, "Lỗi",  "Không thể đăng nhập", null)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
                    String msg = AlertUtils.getMessageThrowable(LoginActivity.this, t);
                    Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
                }
            });
    }

    private void startMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_login){
            if (!Utilities.isNetworkAvailable(this)){
                Toast.makeText(this, "Không có kết nối mạng", Toast.LENGTH_SHORT).show();
                return;
            }
            else if (TextUtils.isEmpty(txtUserID.getText())){
                AlertUtils.getAlertClose(this, "Thiếu dữ liệu", "Tên đăng nhập không được rỗng", null)
                        .show();
                return;
            }
            else if (TextUtils.isEmpty(txtPwd.getText())){
                AlertUtils.getAlertClose(this, "Thiếu dữ liệu", "Mật khẩu không được rỗng", null)
                        .show();
                return;
            } else {
                final String userName = txtUserID.getText().toString();
                final String pwd = txtPwd.getText().toString();
                login(userName, pwd);
            }
        }
        else if (v.getId() == R.id.img_setting){
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
        }
    }

    public boolean hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        return true;
    }
}