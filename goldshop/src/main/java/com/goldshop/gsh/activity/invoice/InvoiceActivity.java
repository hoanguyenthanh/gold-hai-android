package com.goldshop.gsh.activity.invoice;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.BaseActivity;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.callback.DetailCallback;
import com.goldshop.gsh.callback.ProductsCallback;
import com.goldshop.gsh.models.Group;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.layouts.ViewPagerNotSwiping;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class InvoiceActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = InvoiceActivity.class.getName();

    public static int PAGE_INDEX_CHARGING = 0;
    public static int PAGE_INDEX_PRODUCTS = 1;
    public static int PAGE_INDEX_DETAIL = 2;

    private OrderAPI mOrderAPI;

    private ProgressBar mProgressBar;
    private RelativeLayout relativeTopBar;
    private List<Fragment> mListFragment = new ArrayList<>();
    private ViewPagerNotSwiping viewPager;
    private Button btnBack, btnDelete;
    private TextView mTitle;

    private InvoiceProductsFragment invoiceProductsFragment;
    private InvoiceDetailFragment invoiceDetailFragment;
    private InvoiceChargingFragment invoiceChargingFragment;

    private String mTypeTransaction; // Sell | Buy | Exchange
    private long mOrderId;
    private List<Group> mListGroup;
    private Setting mSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.invoice_activity);

        mSetting = new Setting(this);

        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);

        getDataExtras();
        initView();
        getGoldGroups();
    }

    private void getDataExtras(){
        mOrderId = getIntent().getExtras().getLong(Constants.EXTRA_ORDER_ID);
        mTypeTransaction = getIntent().getExtras().getString(Constants.EXTRA_TYPE_TRANS);
    }

    private void initView(){
        mProgressBar = findViewById(R.id.loading_process_bar);
        mProgressBar.setVisibility(View.GONE);
        relativeTopBar = findViewById(R.id.invoice_activity_top_bar);
        mTitle = findViewById(R.id.invoice_activity_title);
        btnBack = findViewById(R.id.invoice_activity_btn_back);
        btnDelete = findViewById(R.id.btn_delete);

        btnBack.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        viewPager = findViewById(R.id.pager);
        viewPager.setPagingEnable(false);
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        if (id == btnBack.getId()){
            if (viewPager.getCurrentItem() == InvoiceActivity.PAGE_INDEX_PRODUCTS) {
                onBackPressed();
            } else {
                viewPager.setCurrentItem(InvoiceActivity.PAGE_INDEX_PRODUCTS);
            }
        }
        else if (id == btnDelete.getId()){
            if (invoiceDetailFragment != null) invoiceDetailFragment.delete();
        }
    }

    @Override
    public void onBackPressed() {

        if (viewPager != null){
            if (viewPager.getCurrentItem() == PAGE_INDEX_CHARGING) {
                viewPager.setCurrentItem(PAGE_INDEX_PRODUCTS);
            }
            else if (viewPager.getCurrentItem() == PAGE_INDEX_DETAIL) {
                if (invoiceDetailFragment != null) {
                    if (invoiceDetailFragment.isNumberInputShowing()) {
                        invoiceDetailFragment.hideNumberInput();
                    } else {
                        viewPager.setCurrentItem(PAGE_INDEX_PRODUCTS);
                    }
                }
                else {
                    viewPager.setCurrentItem(PAGE_INDEX_PRODUCTS);
                }
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }

    }

    private void initFragments(){
        invoiceProductsFragment = InvoiceProductsFragment.newInstance(this.mOrderId, mTypeTransaction, new ProductsCallback() {
            @Override
            public void itemClick(OrderDetail orderDetail) {

                if (orderDetail != null && invoiceDetailFragment != null){

                    invoiceDetailFragment.setTypeTransaction(mTypeTransaction);
                    invoiceDetailFragment.setOrderDetail(orderDetail);

                    if (viewPager != null){
                        viewPager.setCurrentItem(PAGE_INDEX_DETAIL);
                    }
                }
            }

            @Override
            public void addClick(String action) {
                if (invoiceDetailFragment != null) {

                    //Không được thay đổi thứ tự
                    invoiceDetailFragment.setAction(action);
                    invoiceDetailFragment.setOrderId(mOrderId);
                    invoiceDetailFragment.setOrderDetail(null);
                    invoiceDetailFragment.setTypeTransaction(mTypeTransaction);

                    if (viewPager != null) {
                        viewPager.setCurrentItem(PAGE_INDEX_DETAIL);
                    }
                }
            }

            @Override
            public void chargingClick() {
                if (invoiceChargingFragment != null){
                    invoiceChargingFragment.setOrderId(mOrderId);
                    invoiceChargingFragment.setTypeTransaction(mTypeTransaction);

                    if (viewPager != null){
                        viewPager.setCurrentItem(PAGE_INDEX_CHARGING);
                    }
                }
            }

            @Override
            public void startCamera(boolean isStart) {

            }
        });

        invoiceDetailFragment = InvoiceDetailFragment.getInstance(mOrderId, mListGroup, mTypeTransaction, new DetailCallback() {
            @Override
            public void saveSuccess(long orderId, long orderDetailId) {
                mOrderId = orderId;
                if (invoiceProductsFragment != null) {
                    invoiceProductsFragment.setOrderId(mOrderId);
                }
            }

            @Override
            public void chargingClick() {
                if (invoiceChargingFragment != null){
                    invoiceChargingFragment.setOrderId(mOrderId);
                    invoiceChargingFragment.setTypeTransaction(mTypeTransaction);

                    if (viewPager != null){
                        viewPager.setCurrentItem(PAGE_INDEX_CHARGING);
                    }
                }
            }
        });

        invoiceChargingFragment = InvoiceChargingFragment.newInstance();

        mListFragment.add(invoiceChargingFragment);
        mListFragment.add(invoiceProductsFragment);
        mListFragment.add(invoiceDetailFragment);

        PagerAdapter adapter = new PagerAdapter(this.getSupportFragmentManager(), mListFragment.size(), mListFragment);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(PAGE_INDEX_PRODUCTS);

        setPagerListener();
    }

    public void setVisibleDeleteButton(boolean visible){
        if (btnDelete != null)  btnDelete.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void getGoldGroups() {
        mProgressBar.setVisibility(View.VISIBLE);

        mOrderAPI.getGroup().enqueue(new Callback<List<Group>>() {
            @Override
            public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {
                if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
                if (!isFinishing()) {
                    if (response.isSuccessful()) {
                        mListGroup = response.body();
                        if (mListGroup != null) {
                            initFragments();
                        }
                    } else {
                        Toast.makeText(InvoiceActivity.this, response.message(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Group>> call, Throwable t) {
                if (mProgressBar != null) mProgressBar.setVisibility(View.GONE);
                if (!isFinishing()) {
                    Toast.makeText(InvoiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void setPagerListener(){
        invoiceChargingFragment.setOnPagingListener(position -> {
            if (viewPager != null){
                viewPager.setCurrentItem(position);
            }
        });

        invoiceDetailFragment.setOnPagingListener(position -> {
            if (viewPager != null){
                viewPager.setCurrentItem(position);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                relativeTopBar.setBackgroundResource(R.color.colorOrangePrimary01);
                mTitle.setTextColor(getResources().getColor(R.color.colorBlack));

                if (position == PAGE_INDEX_PRODUCTS){
                    if (mTypeTransaction.equals(Constants.SELL)) {
                        mTitle.setText(getString(R.string.str_title_list_product_sell));
                    }
                    else if (mTypeTransaction.equals(Constants.BUY)){
                        mTitle.setText(getString(R.string.str_title_list_product_buy));
                    }
                    else {
                        mTitle.setText(getString(R.string.str_title_list_product_sell_and_buy));
                    }

                    btnBack.setVisibility(View.VISIBLE);
                }
                else if (position == PAGE_INDEX_DETAIL){
                    if (mTypeTransaction.equals(Constants.SELL)) {
                        mTitle.setText(getString(R.string.str_title_detail_product_sell));
                    }
                    else if (mTypeTransaction.equals(Constants.BUY)){
                        mTitle.setText(getString(R.string.str_title_detail_product_buy));
                    }
                    else {
                        mTitle.setText(getString(R.string.str_title_detail_product_sell_and_buy));
                    }

                    btnBack.setVisibility(View.VISIBLE);
                }
                else if (position == PAGE_INDEX_CHARGING){
                    if (mTypeTransaction.equals(Constants.SELL)) {
                        mTitle.setText(getString(R.string.str_title_charging_product_sell));
                    }
                    else if (mTypeTransaction.equals(Constants.BUY)){
                        mTitle.setText(getString(R.string.str_title_charging_product_buy));
                    }
                    else {
                        mTitle.setText(getString(R.string.str_title_charging_product_sell_and_buy));
                    }

                    btnBack.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected: " + position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private class PagerAdapter extends FragmentStatePagerAdapter {
        private int mNumOfTabs;
        private List<Fragment> lstFragments = new ArrayList<Fragment>();

        public PagerAdapter(FragmentManager fm, int NumOfTabs, List<Fragment> list) {
            super(fm);
            this.lstFragments = list;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            return lstFragments.get(position);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
