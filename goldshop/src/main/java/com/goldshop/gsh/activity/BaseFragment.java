package com.goldshop.gsh.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import com.goldshop.gsh.R;


/**
 * Created by HOA on 08/07/2017.
 */

public class BaseFragment extends Fragment {
    private Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    public void showAlertDialog(@NonNull String title,@NonNull String msg){
        if (mContext == null) return;

        new AlertDialog.Builder(mContext)
            .setPositiveButton(mContext.getString(R.string.str_close), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            })
            .setTitle(title)
            .setMessage(msg)
            .create()
            .show();
    }

    public void showConfirmDialog(@NonNull String title,@NonNull String msg,
                                  DialogInterface.OnClickListener negativeButton, DialogInterface.OnClickListener positiveButton){
        new AlertDialog.Builder(mContext)
            .setTitle(title)
            .setMessage(msg)
            .setNegativeButton(R.string.str_ok, negativeButton)
            .setPositiveButton(R.string.str_close, positiveButton)
            .create()
            .show();
    }
}
