package com.goldshop.gsh.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.invoice.InvoiceActivity;
import com.goldshop.gsh.adapter.ListInvoiceAdapter;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.CommonResponse;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.callback.AcceptCallback;
import com.goldshop.gsh.callback.CloseCallback;
import com.goldshop.gsh.models.Order;
import com.goldshop.gsh.models.User;
import com.goldshop.gsh.utils.AlertUtils;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.RecyclerViewItemClickSupport;
import com.goldshop.gsh.utils.Setting;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListInvoiceActivity extends BaseActivity {
    private OrderAPI orderAPI;
    private List<Order> listOrder;
    private RecyclerView mRecyclerView;
    private ListInvoiceAdapter listInvoiceAdapter;
    private Button btnBack;

    private Context context;
    private Setting mSetting;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.list_invoice_activity);

        context = this;

        mSetting = new Setting(context);
        orderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
        user = mSetting.getUserInfo();

        mProgressBar = findViewById(R.id.loading_process_bar);
        mProgressBar.setVisibility(View.GONE);

        mRecyclerView = findViewById(R.id.list_invoice_activity_list_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerViewItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new RecyclerViewItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                if (listOrder != null && position < listOrder.size()) {
                    long orderId = listOrder.get(position).OrderId;
                    showAlert(orderId);
                }
                return false;
            }
        });

        RecyclerViewItemClickSupport.addTo(mRecyclerView).setOnItemClickListener((recyclerView, position, v) -> {

            if (position == 0) return;

            String typeTrans = listOrder.get(position).getTypeTrans();
            long id = listOrder.get(position).getOrderId();

            Intent intent = new Intent(ListInvoiceActivity.this, InvoiceActivity.class);
            intent.putExtra(Constants.EXTRA_NAME_CLASS, ListInvoiceActivity.class.toString());

            if (Constants.BUY.equals(typeTrans)) {
                intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.BUY);
                intent.putExtra(Constants.EXTRA_ORDER_ID, id);
                startActivity(intent);
            }
            else if (Constants.SELL.equals(typeTrans)) {
                intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.SELL);
                intent.putExtra(Constants.EXTRA_ORDER_ID, id);
                startActivity(intent);
            }
            else if (Constants.SELL_BUY.equals(typeTrans)) {
                intent.putExtra(Constants.EXTRA_TYPE_TRANS, Constants.SELL_BUY);
                intent.putExtra(Constants.EXTRA_ORDER_ID, id);
                startActivity(intent);
            }
        });

        btnBack = findViewById(R.id.list_invoice_activity_btn_back);
        btnBack.setOnClickListener(v -> ListInvoiceActivity.this.finish());
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadOrder();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        RecyclerViewItemClickSupport.removeFrom(mRecyclerView);
    }


    private void showAlert(long orderId){
        String[] actions = {context.getString(R.string.str_to_status_new),
                context.getString(R.string.str_to_status_print),
                context.getString(R.string.str_delete_invoice_this)};

        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setTitle(context.getString(R.string.str_action))
                .setItems(actions, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                if (user != null && user.isAdmin()){
                                    updateStatus(orderId, Constants.STATUS_NEW);
                                } else {
                                    AlertUtils.showToastLong(context, context.getString(R.string.str_not_grand));
                                }
                                break;
                            case 1:
                                if (user != null && user.isAdmin()){
                                    updateStatus(orderId, Constants.STATUS_PRINTED);
                                } else {
                                    AlertUtils.showToastShort(context, context.getString(R.string.str_not_grand));
                                }
                                break;
                            case 2:
                                showAlertDelete(orderId);
                                break;

                        }
                    }
                });
        builder.create().show();
    }

    private void hideLoading(){
        if (mProgressBar != null){
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void showLoading(){
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void initAdapter(){
        if (listInvoiceAdapter == null) {
            listInvoiceAdapter = new ListInvoiceAdapter(listOrder);
            mRecyclerView.setAdapter(listInvoiceAdapter);
        } else {
            listInvoiceAdapter.setListOrder(listOrder);
        }
    }

    private void loadOrder(){
        showLoading();

        if (listOrder == null){
            listOrder = new ArrayList<>();
        } else {
            listOrder.clear();
        }

        orderAPI.getOrderList().enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(Call<List<Order>> call, Response<List<Order>> response) {
                if (!isFinishing()) {
                    hideLoading();

                    if (response.isSuccessful()) {
                        listOrder = response.body();
                        initAdapter();
                    } else {
                        Toast.makeText(ListInvoiceActivity.this, response.message(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Order>> call, Throwable t) {
                if (!isFinishing()) {
                    Toast.makeText(ListInvoiceActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    hideLoading();
                }
            }
        });
    }

    private void updateStatus(long orderId, String status){

        showLoading();

        orderAPI.updateStatusOrder(orderId, status).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                hideLoading();

                if (response.isSuccessful()){
                    loadOrder();
                    AlertUtils.showToastShort(context, context.getString(R.string.str_update_status_successful));
                } else {
                    AlertUtils.showErrorResponse(context, response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {

                hideLoading();

                String error = AlertUtils.getMessageThrowable(context, t);
                AlertUtils.showToastShort(getApplicationContext(), error);
            }
        });
    }


    private void showAlertDelete(final long orderId){
        AlertUtils.getAlertOKCancel(context, context.getString(R.string.str_confirm_delete),
                "Bạn chắc muốn xóa hóa đơn nãy vĩnh viễn?",
                new CloseCallback() {
                    @Override
                    public void onClose() {

                    }
                }
                , new AcceptCallback() {
                    @Override
                    public void accept() {
                        deleteOrder(orderId);
                    }
                })
                .show();
    }


    private void deleteOrder(long orderId){
        if (orderId > 0){
            showLoading();

            orderAPI.deleteOrder(orderId).enqueue(new Callback<CommonResponse>() {

                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        hideLoading();
                        CommonResponse resp = response.body();

                        if (response.isSuccessful()) {
                            if (resp.msgCode > 0){
                                loadOrder();
                                Toast.makeText(context, R.string.str_delete_complete, Toast.LENGTH_SHORT).show();
                            } else{
                                AlertUtils.showToastLong(context, resp.msgString);
                            }
                        } else {
                            AlertUtils.showToastLong(context, resp.msgString);
                        }
                    }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    hideLoading();

                    String message = AlertUtils.getMessageThrowable(context, t);
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
