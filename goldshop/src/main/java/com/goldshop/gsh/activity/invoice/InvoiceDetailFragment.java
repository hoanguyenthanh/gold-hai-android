package com.goldshop.gsh.activity.invoice;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.BuildConfig;
import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.BaseFragment;
import com.goldshop.gsh.activity.GoldTypeDialogFragment;
import com.goldshop.gsh.activity.SearchProductFragment;
import com.goldshop.gsh.activity.ZBarScannerActivity;
import com.goldshop.gsh.adapter.InvoiceListBinAdapter;
import com.goldshop.gsh.adapter.InvoiceListGroupAdapter;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.CommonResponse;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.callback.AcceptCallback;
import com.goldshop.gsh.callback.CloseCallback;
import com.goldshop.gsh.callback.DetailCallback;
import com.goldshop.gsh.callback.OnPagingListener;
import com.goldshop.gsh.callback.SearchCodeCallback;
import com.goldshop.gsh.models.GoldType;
import com.goldshop.gsh.models.Group;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.models.ProductScanner;
import com.goldshop.gsh.models.ProductSearch;
import com.goldshop.gsh.models.ResultSaveOrder;
import com.goldshop.gsh.utils.AlertUtils;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.GoldUtils;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.Utilities;
import com.goldshop.gsh.utils.layouts.KeyBoard;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceDetailFragment extends BaseFragment
        implements View.OnClickListener, KeyBoard.OnListener, View.OnTouchListener {
    public static final String TAG = InvoiceDetailFragment.class.getSimpleName();

    private OrderAPI mOrderAPI;
    private Setting mSetting;
    private FragmentActivity mContext;

    private ConstraintLayout mConstrainBottom;
    private ProgressBar mProgressBar;
    private LinearLayout llScanner;
    private ImageView btnScanner;
    private Button btnSave, btnCharging, btnDownload, btnSelectGoldType;
    private KeyBoard mKeyboard;

    private LinearLayout llMain;
    private LinearLayout linearProductCode, linearGoldGroup, linearWeightDiamond, linearWeightTotal;
    private LinearLayout linearSalary, linearSurcharge;
    private EditText editProductCode;
    private EditText editProdDesc, editSurcharge, editSalary, editWeightGold, editWeightDiamond, editWeightTotal;
    private EditText editPrice, editAmount;
    private TextView tvStatus;
    private TextView tvGoldType, tvGoldTypeDesc;
    private WeightGoldView txtWeightGoldDesc, txtWeightDiamondDesc, txtWeightTotal;
    private TextView lblPrice;
    private Spinner spnBin, spnGroup;

    private InvoiceListBinAdapter binAdapter;
    private InvoiceListGroupAdapter groupAdapter;

    private ProductScanner productScanner;
    private OnPagingListener onPagingListener;
    private DetailCallback onDetailCallback;

    private List<Group> goldGroupList = new ArrayList<>();

    private boolean saveAndGoCharging = false;
    private String mGoldTypeExchange;
    private String mTypeTransaction; // Type transaction for each Invoice
    private String mActionTrans; //action add sell, add buy fro each product

    private long mOrderId;
    private long mOrderDetailId;
    private OrderDetail mOrderDetail;

    public static InvoiceDetailFragment getInstance(long orderId, @NonNull List<Group> lstGroup, @NonNull String typeTrans, DetailCallback callBack){
        InvoiceDetailFragment fragment = new InvoiceDetailFragment();

        fragment.mOrderId = orderId;
        fragment.goldGroupList = lstGroup;
        fragment.mTypeTransaction = typeTrans;
        fragment.onDetailCallback = callBack;

        return fragment;
    }

    public void setOrderId(long _orderId){
        this.mOrderId = _orderId;
    }

    public void setOrderDetail(OrderDetail detail){
        mOrderDetail = detail;
        if (mOrderDetail != null){
            mActionTrans = mOrderDetail.TypeTrans;
            mOrderDetailId = detail.OrderDetailId;
        } else {
            mOrderDetailId = 0;
        }
    }

    public void setTypeTransaction(String typeTrans){
        mTypeTransaction = typeTrans;
    }


    public void setAction(String action){
        this.mActionTrans = action;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mSetting = new Setting(getContext());
        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            getStatusOrder();
            bindValue2Text();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.invoice_detail_fragment, container, false);

        llMain = mView.findViewById(R.id.linear_input_board);
        mKeyboard = mView.findViewById(R.id.invoice_detail_keyboard);
        mConstrainBottom = mView.findViewById(R.id.linear_bottom);
        llScanner = mView.findViewById(R.id.invoice_detail_llScanner);

        mKeyboard.setVisibility(View.GONE);

        btnScanner = mView.findViewById(R.id.invoice_detail_btn_scanner);
        btnSave = mView.findViewById(R.id.invoice_detail_btn_save);
        btnCharging = mView.findViewById(R.id.invoice_detail_btn_charging);
        btnDownload = mView.findViewById(R.id.invoice_detail_btn_download);
        btnSelectGoldType = mView.findViewById(R.id.btn_select_gold_type);

        linearProductCode = mView.findViewById(R.id.linear_product_code);
        linearGoldGroup = mView.findViewById(R.id.linear_gold_group);
        linearWeightDiamond = mView.findViewById(R.id.linear_weight_diamond);
        linearWeightTotal = mView.findViewById(R.id.linear_weight_total);
        linearSalary = mView.findViewById(R.id.linear_salary);
        linearSurcharge = mView.findViewById(R.id.linear_surcharge);

        spnBin = mView.findViewById(R.id.spinner_bin);
        spnGroup = mView.findViewById(R.id.spinner_group);

        tvGoldType = mView.findViewById(R.id.tv_gold_type);
        tvGoldTypeDesc = mView.findViewById(R.id.tv_gold_type_desc);

        editProductCode = mView.findViewById(R.id.edit_product_code_auto_complete);
        editProdDesc = mView.findViewById(R.id.edit_product_desc);

        editSalary = mView.findViewById(R.id.edit_salary);
        editSurcharge = mView.findViewById(R.id.edit_surcharge);
        editWeightGold = mView.findViewById(R.id.edit_weight_gold);
        editWeightDiamond = mView.findViewById(R.id.edit_weight_diamond);
        editWeightTotal = mView.findViewById(R.id.edit_weight_total);

        lblPrice = mView.findViewById(R.id.txt_price);
        editPrice = mView.findViewById(R.id.edit_price);
        editAmount = mView.findViewById(R.id.edit_amount);

        txtWeightGoldDesc = mView.findViewById(R.id.edit_weight_gold_desc);
        txtWeightDiamondDesc = mView.findViewById(R.id.edit_weight_diamond_desc);
        txtWeightTotal = mView.findViewById(R.id.txt_weight_total_desc);

        tvStatus = mView.findViewById(R.id.txt_status);

        mProgressBar = mView.findViewById(R.id.invoice_detail_process_bar);
        mProgressBar.setVisibility(View.GONE);

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setDataAdapter();
        setListener();
    }

    private void bindValue2Text(){
        if (mOrderDetail != null){
            OrderDetail detail = mOrderDetail;

            String strBin = detail.getDepot();
            String strType = detail.getGoldType();
            String strGroup = detail.getGroupID();

            int indexBin = getIndexSpinnerBin(strBin);
            if (indexBin >= 0) {
                spnBin.setSelection(indexBin);
            }

            int indexGroup = getIndexSpinnerGroup(strGroup);
            if (indexGroup >= 0) {
                spnGroup.setSelection(indexGroup);
            }


            tvGoldType.setText(strType);
            tvGoldTypeDesc.setText(detail.getGoldDesc());
            editPrice.setText(Utilities.trimZeroRight(detail.getPriceDT()));


            editProductCode.setText(Utilities.replaceStringNull(detail.getProductCode()));
            editProdDesc.setText(Utilities.replaceStringNull(detail.getProductDesc()));
            editSalary.setText(Utilities.trimZeroRight(detail.getSalary()));
            editSurcharge.setText(Utilities.trimZeroRight(detail.getPayBonus()));

            editWeightGold.setText(Utilities.trimZeroRight(detail.getTotalWeightDT()));
            editWeightDiamond.setText(Utilities.trimZeroRight(detail.getDiamondWeight()));
            editWeightTotal.setText(Utilities.trimZeroRight(detail.getTotalWeightDT()));

            int intWeightGold = (int)(detail.getTotalWeightDT() - detail.getDiamondWeight());
            int intWeightDiamond = (int)detail.getDiamondWeight();
            int intTotalWeightDT = detail.getTotalWeightDT();
            txtWeightGoldDesc.setText(intWeightGold);
            txtWeightDiamondDesc.setText(intWeightDiamond);
            txtWeightTotal.setText(intTotalWeightDT);
        } else {
            editProductCode.setText(Constants.EMPTY);
            editProdDesc.setText(Constants.EMPTY);
            editSalary.setText(Constants.EMPTY);
            editSurcharge.setText(Constants.EMPTY);
            editWeightGold.setText(Constants.EMPTY);
            editWeightDiamond.setText(Constants.EMPTY);
            editWeightTotal.setText(Constants.EMPTY);
            editPrice.setText(Constants.EMPTY);

            txtWeightGoldDesc.setText(0);
            txtWeightDiamondDesc.setText(0);
            txtWeightTotal.setText(0);


            tvGoldType.setText(Constants.EMPTY);
            tvGoldTypeDesc.setText(Constants.EMPTY);
        }

        //Set visible items
        if (Constants.SELL.equals(mActionTrans)) {

            linearProductCode.setVisibility(View.VISIBLE);
            linearWeightDiamond.setVisibility(View.VISIBLE);
            linearWeightTotal.setVisibility(View.VISIBLE);
            linearGoldGroup.setVisibility(View.VISIBLE);
            linearSurcharge.setVisibility(View.GONE);
            linearSalary.setVisibility(View.VISIBLE);

            llScanner.setVisibility(View.VISIBLE);
        }
        else if (Constants.BUY.equals(mActionTrans)) {

            linearProductCode.setVisibility(View.GONE);
            linearWeightDiamond.setVisibility(View.GONE);
            linearWeightTotal.setVisibility(View.GONE);
            linearGoldGroup.setVisibility(View.GONE);
            linearSalary.setVisibility(View.GONE);
            linearSurcharge.setVisibility(View.VISIBLE);

            if (editProdDesc.getText().toString().length() == 0){
                editProdDesc.setText(getString(R.string.str_old_gold));
            }
            llScanner.setVisibility(View.GONE);
        } else {
            //mActionTrans Unknow then hide
            linearProductCode.setVisibility(View.GONE);
            linearWeightDiamond.setVisibility(View.GONE);
            linearWeightTotal.setVisibility(View.GONE);
            linearGoldGroup.setVisibility(View.GONE);
            linearSalary.setVisibility(View.GONE);
        }

        if (Constants.SELL_BUY.equals(mTypeTransaction)){
            btnCharging.setVisibility(View.INVISIBLE);
            btnSelectGoldType.setVisibility(mOrderDetailId > 0 ? View.GONE : View.VISIBLE);
        } else {
            btnCharging.setVisibility(View.VISIBLE);
            btnSelectGoldType.setVisibility(View.VISIBLE);
        }
    }

    private void enableInput(boolean enable){

        editSalary.setEnabled(enable);
        editSurcharge.setEnabled(enable);
        editWeightGold.setEnabled(enable);
        editWeightDiamond.setEnabled(enable);
        editWeightTotal.setEnabled(enable);

        editPrice.setEnabled(enable);
        editAmount.setEnabled(enable);
        editProductCode.setEnabled(enable);
        editProdDesc.setEnabled(enable);

        btnDownload.setEnabled(enable);
        btnSelectGoldType.setEnabled(enable);

        spnBin.setEnabled(enable);
        spnGroup.setEnabled(enable);
    }

    private void setListener(){
        btnScanner.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        btnCharging.setOnClickListener(this);
        btnDownload.setOnClickListener(this);
        btnSelectGoldType.setOnClickListener(this);

        editSalary.setOnClickListener(this);
        editSurcharge.setOnClickListener(this);
        editWeightGold.setOnClickListener(this);
        editWeightDiamond.setOnClickListener(this);
        editWeightTotal.setOnClickListener(this);

        editProductCode.setOnTouchListener(this);
        editProdDesc.setOnTouchListener(this);

        editWeightGold.setOnTouchListener(this);
        editWeightDiamond.setOnTouchListener(this);
        editWeightTotal.setOnTouchListener(this);
        editPrice.setOnTouchListener(this);
        editSalary.setOnTouchListener(this);
        editSurcharge.setOnTouchListener(this);

        editWeightGold.addTextChangedListener(amountTextWatcher);
        editWeightDiamond.addTextChangedListener(amountTextWatcher);
        editSalary.addTextChangedListener(amountTextWatcher);
        editSurcharge.addTextChangedListener(amountTextWatcher);
        editPrice.addTextChangedListener(amountTextWatcher);

        mKeyboard.setOnListener(this);
        llMain.setOnClickListener(this);
    }

    /**
     * If Price > 0 get that. Opposite get from DTO
     */
    private void setValuePrice(GoldType curType, double price){
        double priceSell = 0.0;
        double priceBuy = 0.0;

        if (price > 0) {
            priceSell = price;
            priceBuy = price;
        }
        else if (curType != null && curType.PriceSell > 0 && curType.PriceBuy > 0){
            priceSell = curType.PriceSell;
            priceBuy = curType.PriceBuy;
        }

        if (priceSell > 0 && priceBuy > 0) {
            lblPrice.setText(getString(R.string.str_not_exists_price));
            editPrice.setText(Constants.ZERO);

            if (Constants.SELL.equals(mActionTrans)) {
                lblPrice.setText(getString(R.string.str_price_sell));
                editPrice.setText(Utilities.trimZeroRight(priceSell));
            }
            else if (Constants.BUY.equals(mActionTrans)) {
                lblPrice.setText(getString(R.string.str_price_buy));
                editPrice.setText(Utilities.trimZeroRight(priceBuy));
            }
        } else {
            enableInput(false);
            AlertUtils.showToastLong(mContext, mContext.getString(R.string.str_price_gold_not_setting));
        }
    }

    private TextWatcher amountTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            setAmount();
        }
    };

    private void setAmount(){
        double weightGold = 0;
        double weightDiamond = 0;
        double price = 0;
        double salary = 0;
        double bonusPay = 0;

        if (editWeightGold.getText().toString().length() > 0){
            weightGold = Double.valueOf(editWeightGold.getText().toString());
        }
        if (editWeightDiamond.getText().toString().length() > 0){
            weightDiamond = Double.valueOf(editWeightDiamond.getText().toString());
        }
        if (editPrice.getText().toString().length() > 0){
            price = Double.valueOf(editPrice.getText().toString());
        }
        if (editSalary.getText().toString().length() > 0) {
            salary = Double.valueOf(editSalary.getText().toString());
        }
        if (editSurcharge.getText().toString().length() > 0){
            bonusPay = Double.valueOf(editSurcharge.getText().toString());
        }

        double weightTotal = weightGold + weightDiamond;
        editWeightTotal.setText(Utilities.trimZeroRight(weightTotal));
        double amount = ((weightGold / 100.0) * price) + salary - (bonusPay * (weightGold / 100.0));
        editAmount.setText(Utilities.trimZeroRight(amount));
    }

    private void setDataAdapter(){
        binAdapter = new InvoiceListBinAdapter(getActivity());
        spnBin.setAdapter(binAdapter);

        groupAdapter = new InvoiceListGroupAdapter(getActivity(), goldGroupList);
        spnGroup.setAdapter(groupAdapter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == btnSave.getId()){
            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }

            //Check price gold
            double price = 0;
            String sPrice = editPrice.getText().toString().replace("", "");
            if (!TextUtils.isEmpty(sPrice)) {
                price = Double.valueOf(sPrice);
            }
            if (price == 0){
                showAlertDialog(getString(R.string.str_err_price_gold), getString(R.string.str_price_gold_not_setting));
                return;
            }

            if (TextUtils.isEmpty(editProdDesc.getText().toString()) || TextUtils.isEmpty(editWeightGold.getText().toString())){
                showAlertDialog(getString(R.string.str_msg_notify), getString(R.string.str_msg_010));
                return;
            }

            if (mOrderDetailId == 0) {
                checkGoldTypeExchange(Constants.METHOD_INSERT);
            }
            else if (mOrderDetailId > 0) {
                checkGoldTypeExchange(Constants.METHOD_UPDATE);
            } else {
                Toast.makeText(getActivity(), getString(R.string.str_msg_001), Toast.LENGTH_LONG).show();
            }
        }
        else if (id == btnCharging.getId()){
            saveAndGoCharging = true;
            btnSave.performClick();
        }
        else if (id == btnScanner.getId()){;
            launchActivity(ZBarScannerActivity.class);
        }
        else if (id == btnDownload.getId()){
            if (getActivity() != null && getActivity().getSupportFragmentManager() != null) {
                if (getView() != null) {
                    Utilities.hideKeySoft(getView(), getContext());
                }

                SearchProductFragment codeFragment = SearchProductFragment.newInstance(new SearchCodeCallback() {
                    @Override
                    public void selectData(ProductSearch codeSearch) {
                        if (editWeightGold != null){
                            editWeightGold.setText("");
                        }
                        if (editWeightTotal != null){
                            editWeightTotal.setText("");
                        }
                        if (editWeightDiamond != null){
                            editWeightDiamond.setText("");
                        }
                        if (editSalary != null){
                            editSalary.setText("");
                        }

                        if (codeSearch != null && !TextUtils.isEmpty(codeSearch.ProductCode)){
                            getProductScanner(codeSearch.ProductCode);
                        }
                    }
                });

                getActivity().getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.push_up_in, R.anim.push_down_out)
                        .add(R.id.invoice_activity_fragment, codeFragment, SearchProductFragment.TAG)
                        .addToBackStack(null)
                        .commit();
            }
        }
        else if (id == btnSelectGoldType.getId()){
            GoldTypeDialogFragment fragment = GoldTypeDialogFragment.newInstance(tvGoldType.getText().toString(),
                    new GoldTypeDialogFragment.OnGoldTypeSelect() {
                        @Override
                        public void onSelect(GoldType goldType) {
                            if (goldType != null && tvGoldType != null && tvGoldTypeDesc != null){
                                tvGoldType.setText(goldType.GoldCode);
                                tvGoldTypeDesc.setText(goldType.GoldDesc);
                                setValuePrice(goldType, 0);
                            }
                        }
                    });

            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(fragment, fragment.TAG)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        else if (id == llMain.getId()){
            hideNumberInput();
            hideKeySoft();
        }
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        int inType = 0;

        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (id == editProdDesc.getId()){
                hideNumberInput();
                return false;
            }
            else if (id == editProductCode.getId()){
                hideNumberInput();
                return false;
            }
            else if (id == editWeightGold.getId()) {
                hideKeySoft();
                inType = editWeightGold.getInputType();
                editWeightGold.setInputType(InputType.TYPE_NULL);
                editWeightGold.onTouchEvent(event);
                editWeightGold.setInputType(inType);

                showNumberInput(editWeightGold.getId(), editWeightGold.getText().toString());
            }
            else if (id == editWeightDiamond.getId()) {
                hideKeySoft();
                inType = editWeightDiamond.getInputType();
                editWeightDiamond.setInputType(InputType.TYPE_NULL);
                editWeightDiamond.onTouchEvent(event);
                editWeightDiamond.setInputType(inType);

                showNumberInput(editWeightDiamond.getId(), editWeightDiamond.getText().toString());
            }
            else if (id == editWeightTotal.getId()) {
                hideKeySoft();
                inType = editWeightTotal.getInputType();
                editWeightTotal.setInputType(InputType.TYPE_NULL);
                editWeightTotal.onTouchEvent(event);
                editWeightTotal.setInputType(inType);

                showNumberInput(editWeightTotal.getId(), editWeightTotal.getText().toString());
            }
            else if (id == editSurcharge.getId()) {
                hideKeySoft();
                inType = editSurcharge.getInputType();
                editSurcharge.setInputType(InputType.TYPE_NULL);
                editSurcharge.onTouchEvent(event);
                editSurcharge.setInputType(inType);

                showNumberInput(editSurcharge.getId(), editSurcharge.getText().toString());
            }
            else if (id == editSalary.getId()) {
                hideKeySoft();
                // backup the input type
                inType = editSalary.getInputType();
                // disable soft input
                editSalary.setInputType(InputType.TYPE_NULL);
                editSalary.onTouchEvent(event); // call native handler
                editSalary.setInputType(inType); // restore input type

                showNumberInput(editSalary.getId(), editSalary.getText().toString());
            }
            else if (id == editPrice.getId()){
                hideKeySoft();
                // backup the input type
                inType = editPrice.getInputType();
                // disable soft input
                editPrice.setInputType(InputType.TYPE_NULL);
                editPrice.onTouchEvent(event); // call native handler
                editPrice.setInputType(inType); // restore input type

                showNumberInput(editPrice.getId(), editPrice.getText().toString());
            }
            return true;
        }
        return false;
    }

    public void hideKeySoft(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(getActivity().INPUT_METHOD_SERVICE);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        if (getActivity().getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showNumberInput(long targetId, String value){
        this.mKeyboard.setTargetId(targetId, value);
        this.mKeyboard.setVisibility(View.VISIBLE);
        hideBottomMenu();
    }

    public boolean isNumberInputShowing(){
        return mKeyboard.getVisibility() == View.VISIBLE;
    }

    public void hideNumberInput(){
        this.mKeyboard.setVisibility(View.GONE);
        showBottomMenu();
    }

    private void hideBottomMenu(){
        if (mConstrainBottom != null){
            mConstrainBottom.setVisibility(View.GONE);
        }
    }

    public void showBottomMenu(){
        if (mConstrainBottom != null){
            mConstrainBottom.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setValueChanged(long id, String value) {
        if (id == editSurcharge.getId()){
            editSurcharge.setText(value);
            editSurcharge.setSelection(editSurcharge.getText().length());
        }
        else if (id == editSalary.getId()){
            editSalary.setText(value);
            editSalary.setSelection(editSalary.getText().length());
        }
        else if (id == editWeightGold.getId()){
            editWeightGold.setText(value);
            editWeightGold.setSelection(editWeightGold.getText().length());
        }
        else if (id == editWeightDiamond.getId()){
            editWeightDiamond.setText(value);
            editWeightDiamond.setSelection(editWeightDiamond.getText().length());
        }
        else if (id == editWeightTotal.getId()){
            editWeightTotal.setText(value);
            editWeightTotal.setSelection(editWeightTotal.getText().length());
        }
        else if (id == editPrice.getId()){
            editPrice.setText(value);
            editPrice.setSelection(editPrice.getText().length());
        }
    }

    @Override
    public void onCloseWindow(long id){
        this.mKeyboard.setVisibility(View.GONE);
        this.mConstrainBottom.setVisibility(View.VISIBLE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.ACTIVITY_CODE_RESULT_SCANNER){
            if (resultCode == Activity.RESULT_OK && data.getStringExtra(Constants.EXTRA_CODE_PRODUCT) != null){
                String productCode = data.getStringExtra(Constants.EXTRA_CODE_PRODUCT);
                if (BuildConfig.DEBUG){
                    Log.d(TAG, "onActivityResult : productCode = " + productCode);
                }
                if (TextUtils.isEmpty(productCode)){
                    String message = getString(R.string.str_msg_008);
                    if (getActivity() != null) {
                        showAlertDialog(getString(R.string.str_msg_notify), message);
                    }
                }else{
                    getProductScanner(productCode);
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void hideLoading(){
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void showLoading(){
        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void getProductScanner(String productCode){
        this.showLoading();
        final String code = productCode;

        Call<ProductScanner> call;
        if (mSetting.isSearchDataOld() == 1){
            call = mOrderAPI.getProductByCodeOLD(productCode);
        } else {
            call = mOrderAPI.getProductByCode(productCode);
        }

        call.enqueue(new Callback<ProductScanner>() {
            @Override
            public void onResponse(Call<ProductScanner> call, Response<ProductScanner> response) {
                if (!isAdded()){
                    return;
                }

                hideLoading();
                if (response.isSuccessful()){

                    productScanner = response.body();

                    if (productScanner != null && !TextUtils.isEmpty(productScanner.ProductCode)
                            && !TextUtils.isEmpty(productScanner.GoldCode)) {
                        setProductScanner2View(productScanner);
                    } else {
                        Toast.makeText(getActivity(), String.format(getContext().getString(R.string.str_msg_009), code) , Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), String.format(getContext().getString(R.string.str_msg_009), code), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<ProductScanner> call, Throwable t) {
                if (isAdded()) {
                    hideLoading();
                }
            }
        });
    }


    private void setProductScanner2View(ProductScanner dto){
        try {
            int weightAmount;
            int DiamondWeight;
            int TotalWeight;

            //Unit old: L.CPL vd: 1.500 = 1L5C00, 0.600 = 6C00
            if (mSetting.isSearchDataOld() == 1) {
                weightAmount = (int)(dto.GoldWeight * 1000);
                DiamondWeight = (int)(dto.DiamondWeight * 1000);
                TotalWeight = (int)(dto.TotalWeight * 1000);
            } else {
                weightAmount = (int) dto.GoldWeight;
                DiamondWeight = (int) dto.DiamondWeight;
                TotalWeight = (int) dto.TotalWeight;
            }

            editProdDesc.setText(dto.ProductDesc);
            editProductCode.setText(dto.ProductCode);

            editWeightGold.setText(String.valueOf(weightAmount));
            editWeightDiamond.setText(String.valueOf(DiamondWeight));
            editWeightTotal.setText(String.valueOf(TotalWeight));

            tvGoldType.setText(dto.GoldCode);
            tvGoldTypeDesc.setText(dto.GoldDesc == null ? "" : dto.GoldDesc);
            editPrice.setText("0");



            int grpIndex = getIndexSpinnerGroup(dto.GroupID);
            if (grpIndex >= 0) {
                spnGroup.setSelection(grpIndex);
            }
        } catch (Exception e){
            Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_LONG);
            e.printStackTrace();
        }
    }

    private void saveData(final String method){
        Log.d(TAG, "saveData: TypeTrans = " + mTypeTransaction + ", ActionTrans = " + mActionTrans);

        if (!Utilities.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
            return;
        }
        showLoading();

        String user = mSetting.getUserInfo() != null ? mSetting.getUserInfo().UserName : "";
        String TrnID = Constants.EMPTY;
        String prodDesc = editProdDesc.getText().toString();
        String prodCode = editProductCode.getText().toString();

        double diamondWeight = Double.valueOf(editWeightDiamond.getText().toString().length() == 0
                ? "0" : editWeightDiamond.getText().toString());

        int totalWeightDT = Integer.valueOf(editWeightGold.getText().toString().length() == 0
                ? "0" : editWeightGold.getText().toString());

        GoldUtils convertWeightDT = GoldUtils.getInstance(totalWeightDT);
        int totalWeightDT_L = convertWeightDT.getLuongInt();
        int totalWeightDT_C = convertWeightDT.getChiInt();
        int totalWeightDT_P = convertWeightDT.getPhanInt();
        int totalWeightDT_Li = convertWeightDT.getLiInt();

        double priceDT = Double.valueOf(editPrice.getText().toString().length() == 0
                ? "0" : editPrice.getText().toString());

        double payBonus = Double.valueOf(editSurcharge.getText().toString().length() == 0
                ? "0" : editSurcharge.getText().toString());

        double salary = Double.valueOf(editSalary.getText().toString().length() == 0
                ? "0" : editSalary.getText().toString());

        double payBonusAmount = payBonus * (totalWeightDT/100.0);

        double amountDT = Double.valueOf(editAmount.getText().toString().length() == 0
                ? "0" : editAmount.getText().toString());

        String depot = String.valueOf(spnBin.getSelectedItemId()); //Kệ
        String goldType =  tvGoldType.getText().toString();
        String goldGroup = ((Group)spnGroup.getSelectedItem()).GroupID;

        Call<ResultSaveOrder> call = mOrderAPI.saveOrder(method, mTypeTransaction, mActionTrans, mOrderId
                , mOrderDetailId, user, TrnID, prodCode, prodDesc, diamondWeight
                , totalWeightDT
                , totalWeightDT_L
                , totalWeightDT_C
                , totalWeightDT_P
                , totalWeightDT_Li
                , ""
                , priceDT, payBonus, payBonusAmount, salary, amountDT, depot, goldGroup, goldType);

        call.enqueue(new Callback<ResultSaveOrder>() {
            @Override
            public void onResponse(Call<ResultSaveOrder> call, Response<ResultSaveOrder> response) {
                if (!isAdded()){
                    return;
                }
                hideLoading();

                if (response.isSuccessful()){

                    ResultSaveOrder body = response.body();

                    if (body != null) {
                        mOrderId = body.OrderId;
                        mOrderDetailId = body.OrderDetailId;

                        if (!TextUtils.isEmpty(body.ErrorDesc) && body.Result < 0) {
                            showAlertDialog(getString(R.string.str_err_update), body.ErrorDesc);
                        } else {
                            bindValue2Text();
                            Toast.makeText(getActivity(), getString(R.string.str_save_finish), Toast.LENGTH_SHORT).show();

                            //Gold type for all product input case trans exchange
                            mGoldTypeExchange = Constants.SELL_BUY.equals(mTypeTransaction) ? mGoldTypeExchange = goldType : "";

                            if (onDetailCallback != null){
                                onDetailCallback.saveSuccess(mOrderId, mOrderDetailId);
                            }

                            if (saveAndGoCharging) {
                                saveAndGoCharging = false;
                                if (onDetailCallback != null){
                                    onDetailCallback.chargingClick();
                                }
                            } else {
                                onPagingListener.setPageIndex(InvoiceActivity.PAGE_INDEX_PRODUCTS);
                            }
                        }
                    }
                } else {
                    String error = response.toString();
                    if (response.errorBody() != null){
                        try {
                            error = response.errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResultSaveOrder> call, Throwable t) {
                if (isAdded()) {
                    hideLoading();
                    Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public boolean delete(){
        if (!Utilities.isNetworkAvailable(getContext())){
            Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
            return false;
        }
        if (mOrderDetailId == 0){
            Toast.makeText(getContext(), getString(R.string.str_nothing_to_delete), Toast.LENGTH_LONG).show();
            return false;
        }

        AlertDialog dialog = AlertUtils.getAlertOKCancel(mContext,
                getString(R.string.str_confirm_delete),
                getString(R.string.str_confirm_delete_product),
                new CloseCallback() {
                    @Override
                    public void onClose() {

                    }
                },
                new AcceptCallback() {
                    @Override
                    public void accept() {
                        saveData(Constants.METHOD_DELETE);
                    }
                });

        dialog.show();

        return true;
    }

    private int getIndexSpinnerBin(String myString){
        int index = -1;
        for (int i = 0; i < binAdapter.getCount(); i++){
            if (String.valueOf(binAdapter.getItemId(i)).equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }



    private int getIndexSpinnerGroup(String myString){
        int index = -1;
        for (int i = 0; i < groupAdapter.getCount(); i++){
            if (groupAdapter.getItemGroupId(i).equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }


    private void getGoldTypeObject(String goldCode){
        showLoading();

        mOrderAPI.getGoldTypeObject(goldCode).enqueue(new Callback<GoldType>() {
            @Override
            public void onResponse(Call<GoldType> call, Response<GoldType> response) {
                if (isAdded()) {
                    hideLoading();

                    if (response.isSuccessful()) {
                        GoldType goldType = response.body();
                        if (goldType != null){
                            setValuePrice(goldType, 0);
                        }
                    } else {
                        Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<GoldType> call, Throwable t) {
                if (isAdded()) {
                    Toast.makeText(getActivity(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    hideLoading();
                }
            }
        });
    }

    private void getStatusOrder(){
        if (mOrderId == 0) {
            setVisibleDeleteButton(false);
            tvStatus.setText(getActivity().getString(R.string.str_status_new));
            return;
        }

        showLoading();

        mOrderAPI.getStatusOrder(mOrderId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (mContext == null || mContext.isFinishing()) return;
                hideLoading();

                if (response.isSuccessful()){
                    CommonResponse commonResponse = response.body();
                    String status = commonResponse.status == null ? "" : commonResponse.status;

                    tvStatus.setText(Utilities.getStatusName(mContext, status));

                    switch (status){
                        case Constants.STATUS_NEW:
                        case Constants.STATUS_PRINTED:
                            mConstrainBottom.setVisibility(View.VISIBLE);

                            btnSave.setVisibility(View.VISIBLE);
                            btnCharging.setVisibility(View.VISIBLE);
                            btnDownload.setVisibility(View.VISIBLE);
                            llScanner.setVisibility(View.VISIBLE);

                            setVisibleDeleteButton(true);
                            enableInput(true);
                            break;
                        default:
                            mConstrainBottom.setVisibility(View.GONE);
                            btnDownload.setVisibility(View.GONE);
                            setVisibleDeleteButton(false);
                            enableInput(false);
                            break;
                    }
                } else {
                    mConstrainBottom.setVisibility(View.GONE);
                    btnDownload.setVisibility(View.GONE);
                    setVisibleDeleteButton(false);
                    enableInput(false);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (isAdded()) {
                    hideLoading();
                    mConstrainBottom.setVisibility(View.GONE);
                    btnDownload.setVisibility(View.GONE);
                    setVisibleDeleteButton(false);
                    enableInput(false);
                }
            }
        });
    }

    private void checkGoldTypeExchange(final String method){
        if (mOrderId > 0 && Constants.SELL_BUY.equals(mTypeTransaction)){
            String type = tvGoldType.getText().toString();

            mOrderAPI.checkExistGoldType(mOrderId, type).enqueue(new Callback<CommonResponse>() {
                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                    if (response.isSuccessful()){
                        CommonResponse commonResponse = response.body();
                        if (commonResponse.msgCode > 0){
                            saveData(method);
                        }
                        else if (!TextUtils.isEmpty(commonResponse.msgString)) {
                            AlertUtils.getAlertClose(mContext,
                                    mContext.getString(R.string.str_title_error), commonResponse.msgString, null).show();
                        } else {
                            AlertUtils.showToastLong(mContext, mContext.getString(R.string.str_err_update));
                        }
                    } else {
                        AlertUtils.showErrorResponse(mContext, response.errorBody());
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    String msg = AlertUtils.getMessageThrowable(mContext, t);
                    AlertUtils.showToastShort(mContext, msg);
                }
            });
        } else {
            saveData(method);
        }
    }

    private void setVisibleDeleteButton(boolean visible){
        if (getActivity() != null && getActivity() instanceof InvoiceActivity){
            ((InvoiceActivity) getActivity()).setVisibleDeleteButton(visible);
        }
    }

    private static final int ZBAR_CAMERA_PERMISSION = 1;
    private Class<?> mClss;
    public void launchActivity(Class<?> clss) {
        mClss = clss;

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, ZBAR_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(getActivity(), clss);
            startActivityForResult(intent, Constants.ACTIVITY_CODE_RESULT_SCANNER);
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZBAR_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if(mClss != null) {
                        Intent intent = new Intent(getActivity(), mClss);
                        startActivityForResult(intent, Constants.ACTIVITY_CODE_RESULT_SCANNER);
                        startActivity(intent);
                    }
                } else {
                    Toast.makeText(getActivity(), "Camera chưa được gán quyền!", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    public void setOnPagingListener(OnPagingListener listener){
        this.onPagingListener = listener;
    }
}
