package com.goldshop.gsh.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.adapter.ProductSearchAdapter;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.callback.SearchCodeCallback;
import com.goldshop.gsh.models.ProductSearch;
import com.goldshop.gsh.utils.RecyclerViewItemClickSupport;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.Utilities;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by HOA on 17/03/2018.
 */

public class SearchProductFragment extends Fragment {
    public static final String TAG = "SearchProductFragment";

    private static final String txtResult = "Kết quả: ";
    public static final String TYPE_SEARCH_OLD = "old";
    public static final String TYPE_SEARCH_NEW = "new";

    private RecyclerView mRecyclerView;
    private TextView tvTitle;
    private EditText editText, editCode;
    private Button btnSearch;
    private RadioButton radioNew, radioOld;
    private ImageView ivBack;
    private ProductSearchAdapter mSearchCodeAdapter;
    private ProgressBar mProgressBar;

    private SearchCodeCallback mSearchCodeCallback;
    private List<ProductSearch> codeSearchList;

    private Setting mSetting;
    private OrderAPI mOrderAPI;

    public static SearchProductFragment newInstance(SearchCodeCallback codeCallback) {
        Bundle args = new Bundle();
        SearchProductFragment fragment = new SearchProductFragment();
        fragment.setArguments(args);
        fragment.mSearchCodeCallback = codeCallback;
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSetting = new Setting(getActivity());
        mOrderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.search_code_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivBack = view.findViewById(R.id.search_code_back);
        mProgressBar = view.findViewById(R.id.search_code_progressBar);
        tvTitle = view.findViewById(R.id.search_code_title);
        mRecyclerView = view.findViewById(R.id.search_code_RecyclerView);
        btnSearch = view.findViewById(R.id.search_code_btnSearch);
        editText = view.findViewById(R.id.search_code_editTextCode);
        editCode = view.findViewById(R.id.search_code_editCode);

        radioNew = view.findViewById(R.id.search_code_radioNew);
        radioOld = view.findViewById(R.id.search_code_radioOld);

        btnSearch.setOnClickListener(view1 -> {
            String place = radioOld.isChecked() ? TYPE_SEARCH_OLD : TYPE_SEARCH_NEW;
            String text = editText.getText().toString();
            String code = editCode.getText().toString();

            search(place, text, code);
        });

        radioNew.setOnCheckedChangeListener((compoundButton, b) -> {
            radioNew.setChecked(b);
            radioOld.setChecked(!b);
            mSetting.putSearchDataOld(radioOld.isChecked() ? 1 : 0);
        });

        radioOld.setOnCheckedChangeListener((compoundButton, b) -> {
            radioOld.setChecked(b);
            radioNew.setChecked(!b);
            mSetting.putSearchDataOld(radioOld.isChecked() ? 1 : 0);
        });

        if (mSetting.isSearchDataOld() == 1){
            radioOld.setChecked(true);
        } else {
            radioNew.setChecked(true);
        }

        codeSearchList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        RecyclerViewItemClickSupport.addTo(mRecyclerView).setOnItemClickListener((recyclerView, position, v) -> {
            ProductSearch codeSearch = codeSearchList.get(position);

            if (mSearchCodeCallback != null){
                mSearchCodeCallback.selectData(codeSearch);
            }

            if (getActivity() != null && getActivity().getSupportFragmentManager() != null){
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        ivBack.setOnClickListener(view1 -> {
            if (getActivity() != null && getActivity().getSupportFragmentManager() != null){
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    @Override
    public void onDestroy() {
        mSearchCodeCallback = null;
        mSearchCodeAdapter = null;
        super.onDestroy();
    }

    private void search(String place, String text, String code){
        if (getView() != null) {
            Utilities.hideKeySoft(getView(), getContext());
        }
        if (text == null) text = "";
        if (code == null) code = "";
        mProgressBar.setVisibility(View.VISIBLE);

        mOrderAPI.searchProduct(place, text, code).enqueue(new Callback<List<ProductSearch>>() {
            @Override
            public void onResponse(Call<List<ProductSearch>> call, Response<List<ProductSearch>> response) {
                if (getActivity() != null){
                    mProgressBar.setVisibility(View.GONE);

                    if (response.isSuccessful()) {
                        codeSearchList = response.body();

                        if (codeSearchList != null){
                            tvTitle.setText(txtResult + codeSearchList.size());
                        }

                        String typeSearch = mSetting.isSearchDataOld() == 1 ? TYPE_SEARCH_OLD : TYPE_SEARCH_NEW;

                        if (mSearchCodeAdapter == null) {
                            mSearchCodeAdapter = new ProductSearchAdapter(codeSearchList);
                            mSearchCodeAdapter.setTypeSearch(typeSearch);
                            mRecyclerView.setAdapter(mSearchCodeAdapter);
                        } else {
                            mSearchCodeAdapter.setTypeSearch(typeSearch);
                            mSearchCodeAdapter.updateItem(codeSearchList);
                        }
                    } else {
                        try {
                            if (response.errorBody() != null && response.errorBody().string() != null) {
                                Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ProductSearch>> call, Throwable t) {
                if (isAdded()) {
                    mProgressBar.setVisibility(View.GONE);
                }
            }
        });
    }
}
