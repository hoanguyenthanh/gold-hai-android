package com.goldshop.gsh.activity.invoice;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.goldshop.gsh.R;
import com.goldshop.gsh.activity.BaseFragment;
import com.goldshop.gsh.adapter.InvoiceListBuyAdapter;
import com.goldshop.gsh.adapter.InvoiceListSellAdapter;
import com.goldshop.gsh.api.APIClient;
import com.goldshop.gsh.api.CommonResponse;
import com.goldshop.gsh.api.OrderAPI;
import com.goldshop.gsh.callback.AcceptCallback;
import com.goldshop.gsh.callback.CloseCallback;
import com.goldshop.gsh.callback.ProductsCallback;
import com.goldshop.gsh.models.OrderDetail;
import com.goldshop.gsh.utils.AlertUtils;
import com.goldshop.gsh.utils.Constants;
import com.goldshop.gsh.utils.RecyclerViewItemClickSupport;
import com.goldshop.gsh.utils.Setting;
import com.goldshop.gsh.utils.Utilities;
import com.goldshop.gsh.utils.layouts.WeightGoldView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceProductsFragment extends BaseFragment implements View.OnClickListener{

    private Context mContext;

    private View mView;
    private LinearLayout mLinearLoadingProcessBar, llMenuBottom;
    private RecyclerView mListViewSell, mListViewBuy;
    private Button btnDelete, btnCharging;
    private ImageView btnAddSell, btnAddBuy;

    private List<OrderDetail> listOrderSellDto;
    private List<OrderDetail> listOrderBuyDto;
    private List<OrderDetail> listOrderDetail;

    private OrderAPI orderAPI;
    private InvoiceListSellAdapter sellAdapter;
    private InvoiceListBuyAdapter buyAdapter;

    private ProductsCallback mProductCallback;

    private Setting mSetting;

    private String mTypeTrans;
    private String curStatusOrder;
    private long orderId;

    private Handler mHandler = new Handler(Looper.getMainLooper());

    public static InvoiceProductsFragment newInstance(long orderId, String typeTrans, ProductsCallback callback) {
        InvoiceProductsFragment fragment = new InvoiceProductsFragment();
        fragment.orderId = orderId;
        fragment.mTypeTrans = typeTrans;
        fragment.mProductCallback = callback;
        return fragment;
    }

    public void setOrderId(long _orderId){
        this.orderId = _orderId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mSetting = new Setting(getContext());
        orderAPI = APIClient.getInstance(mSetting.getIP(), mSetting.getServerPort()).getRetrofit().create(OrderAPI.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.invoice_products_fragment, container, false);
        mListViewSell =  mView.findViewById(R.id.list_view_sell);
        mListViewBuy =  mView.findViewById(R.id.list_view_buy);
        btnDelete = mView.findViewById(R.id.invoice_btn_del_invoice);
        btnCharging = mView.findViewById(R.id.invoice_btn_charging);
        btnAddSell = mView.findViewById(R.id.linear_container_sell_footer_add);
        btnAddBuy = mView.findViewById(R.id.linear_container_buy_footer_add);

        RecyclerView.LayoutManager mLayoutManagerSell = new LinearLayoutManager(getContext());
        mListViewSell.setLayoutManager(mLayoutManagerSell);
        mListViewSell.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManagerBuy = new LinearLayoutManager(getContext());
        mListViewBuy.setLayoutManager(mLayoutManagerBuy);
        mListViewBuy.setItemAnimator(new DefaultItemAnimator());

        mLinearLoadingProcessBar = mView.findViewById(R.id.linear_loading_process_bar);
        mLinearLoadingProcessBar.setVisibility(View.GONE);

        llMenuBottom = mView.findViewById(R.id.linear_charging);


        listOrderBuyDto = new ArrayList<>();
        listOrderSellDto = new ArrayList<>();

        setVisibleView();
        listener();

        setVisibleDeleteButton(false);

        return mView;
    }

    private void setVisibleDeleteButton(boolean visible){
        if (getActivity() != null && getActivity() instanceof InvoiceActivity){
            ((InvoiceActivity) getActivity()).setVisibleDeleteButton(visible);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser){
            getStatusOrder(orderId);
            loadOrderDetail();
        }
    }

    public void setVisibleView(){
        if (mTypeTrans.equals(Constants.SELL)) {
            mView.findViewById(R.id.linear_container_buy).setVisibility(View.GONE);
            mView.findViewById(R.id.linear_container_buy_footer).setVisibility(View.GONE);
        }
        else if (mTypeTrans.equals(Constants.BUY)){
            mView.findViewById(R.id.linear_container_sell).setVisibility(View.GONE);
            mView.findViewById(R.id.linear_container_sell_footer).setVisibility(View.GONE);
        }
        else if (mTypeTrans.equals(Constants.SELL_BUY)){

        }
    }

    public void setAdapterData(){
        if (sellAdapter == null) {
            sellAdapter = new InvoiceListSellAdapter(listOrderSellDto);
            mListViewSell.setAdapter(sellAdapter);
        } else {
            sellAdapter.updateItem(listOrderSellDto);
            sellAdapter.notifyDataSetChanged();
        }
        if (buyAdapter == null) {
            buyAdapter = new InvoiceListBuyAdapter(listOrderBuyDto);
            mListViewBuy.setAdapter(buyAdapter);
        } else {
            buyAdapter.updateItem(listOrderBuyDto);
            buyAdapter.notifyDataSetChanged();
        }

        double sellTotalWeight = 0.0;
        double sellTotalSalary = 0.0;
        double sellAmountSum = 0.0;

        double buyTotalWeight = 0.0;
        double buyTotalPayBonus = 0.0;
        double buyAmountSum = 0.0;

        if (listOrderSellDto != null && listOrderSellDto.size() > 0){
            for(OrderDetail sell : listOrderSellDto){
                sellTotalWeight += sell.getTotalWeightDT();
                sellTotalSalary += sell.getSalary();
                sellAmountSum += sell.getAmountDT();
            }
        }

        if (listOrderBuyDto != null && listOrderBuyDto.size() > 0){
            for(OrderDetail buy : listOrderBuyDto){
                buyTotalWeight += buy.getTotalWeightDT();
                buyTotalPayBonus += buy.getPayBonusAmount();
                buyAmountSum += buy.getAmountDT();
            }
        }

        try {
            ((TextView) mView.findViewById(R.id.txt_container_sell_footer_salary))
                    .setText(Utilities.moneyFormat(sellTotalSalary));
            ((WeightGoldView) mView.findViewById(R.id.txt_container_sell_footer_weight))
                    .setText(sellTotalWeight);
            ((TextView) mView.findViewById(R.id.txt_container_sell_footer_amount_sum))
                    .setText(Utilities.moneyFormat(sellAmountSum));


            ((TextView) mView.findViewById(R.id.txt_container_buy_footer_pay_bonus))
                    .setText(Utilities.moneyFormat(buyTotalPayBonus));
            ((WeightGoldView) mView.findViewById(R.id.txt_container_buy_footer_weight))
                    .setText(buyTotalWeight);
            ((TextView) mView.findViewById(R.id.txt_container_buy_footer_amount_sum))
                    .setText(Utilities.moneyFormat(buyAmountSum));
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public void listener(){
        RecyclerViewItemClickSupport.addTo(mListViewSell).setOnItemClickListener((recyclerView, position, v) -> {
            if (mProductCallback != null && sellAdapter != null){
                mProductCallback.itemClick(sellAdapter.getItem(position));
            }
        });

        RecyclerViewItemClickSupport.addTo(mListViewBuy).setOnItemClickListener((recyclerView, position, v) -> {
            if (mProductCallback != null && buyAdapter != null){
                mProductCallback.itemClick(buyAdapter.getItem(position));
            }
        });

        btnDelete.setOnClickListener(this);
        btnCharging.setOnClickListener(this);
        btnAddBuy.setOnClickListener(this);
        btnAddSell.setOnClickListener(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == btnDelete.getId()){
            if (orderId == 0){
                return;
            }

            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }

            AlertUtils.getAlertOKCancel(mContext, mContext.getString(R.string.str_confirm_delete),
                    "Bạn chắc muốn xóa hóa đơn nãy vĩnh viễn?",
                    new CloseCallback() {
                        @Override
                        public void onClose() {

                        }
                    }
                    , new AcceptCallback() {
                        @Override
                        public void accept() {
                            deleteOrder();
                        }
                    })
                    .show();

        }
        else if (id == btnCharging.getId()){
            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }
            if (mProductCallback != null){
                mProductCallback.chargingClick();
            }
        }
        else if (id == btnAddSell.getId()){
            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }

            if (mProductCallback != null){
                mProductCallback.addClick(Constants.SELL);
            }
        }
        else if (id == btnAddBuy.getId()){
            if (!Utilities.isNetworkAvailable(getContext())){
                Toast.makeText(getContext(), getString(R.string.str_no_connect_internet), Toast.LENGTH_LONG).show();
                return;
            }

            if (mProductCallback != null){
                mProductCallback.addClick(Constants.BUY);
            }
        }
    }

    private void getStatusOrder(long orderId){
        if (orderId <= 0) return;

        mLinearLoadingProcessBar.setVisibility(View.VISIBLE);
        curStatusOrder = "";

        orderAPI.getStatusOrder(orderId).enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                if (!isAdded() && !isStateSaved()){
                    return;
                }

                mLinearLoadingProcessBar.setVisibility(View.GONE);
                btnCharging.setText(getString(R.string.str_show_charging));

                if (response.isSuccessful()){
                    CommonResponse commonResponse = response.body();
                    curStatusOrder = commonResponse.status == null ? "" : commonResponse.status;

                    switch (curStatusOrder){
                        case Constants.STATUS_NEW:
                        case Constants.STATUS_PRINTED:
                            btnDelete.setVisibility(View.VISIBLE);
                            btnAddBuy.setVisibility(View.VISIBLE);
                            btnAddSell.setVisibility(View.VISIBLE);
                            btnCharging.setText(getString(R.string.str_charging));
                            break;
                        case Constants.STATUS_PAID:
                        case Constants.STATUS_CANCELED:
                            btnDelete.setVisibility(View.INVISIBLE);
                            btnAddBuy.setVisibility(View.INVISIBLE);
                            btnAddSell.setVisibility(View.INVISIBLE);
                        default:
                            btnDelete.setVisibility(View.INVISIBLE);
                            btnAddBuy.setVisibility(View.INVISIBLE);
                            btnAddSell.setVisibility(View.INVISIBLE);
                            break;

                    }
                } else {
                    curStatusOrder = "";
                    btnDelete.setVisibility(View.INVISIBLE);
                    btnAddBuy.setVisibility(View.INVISIBLE);
                    btnAddSell.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                if (!isAdded() && !isStateSaved()){
                    return;
                }
                mLinearLoadingProcessBar.setVisibility(View.GONE);

                curStatusOrder = "";
                btnDelete.setVisibility(View.INVISIBLE);
                btnAddBuy.setVisibility(View.INVISIBLE);
                btnAddSell.setVisibility(View.INVISIBLE);

                btnCharging.setText(getString(R.string.str_show_charging));
            }
        });
    }

    private void loadOrderDetail(){
        if (orderId > 0) {
            llMenuBottom.setVisibility(View.VISIBLE);
            mLinearLoadingProcessBar.setVisibility(View.VISIBLE);

            orderAPI.getOrderDetails(orderId, 0, "").enqueue(new Callback<List<OrderDetail>>() {
                @Override
                public void onResponse(Call<List<OrderDetail>> call, Response<List<OrderDetail>> response) {
                    if (getActivity() != null) {
                        mLinearLoadingProcessBar.setVisibility(View.GONE);

                        listOrderDetail = response.body();
                        listOrderSellDto.clear();
                        listOrderBuyDto.clear();

                        if (listOrderDetail != null && listOrderDetail.size() > 0) {
                            for (OrderDetail dto : listOrderDetail) {
                                if (dto.getTypeTrans().equals(Constants.SELL)) {
                                    listOrderSellDto.add(dto);
                                } else if (dto.getTypeTrans().equals(Constants.BUY)) {
                                    listOrderBuyDto.add(dto);
                                }
                            }
                        }
                        setAdapterData();
                    }
                }

                @Override
                public void onFailure(Call<List<OrderDetail>> call, Throwable t) {
                    if (getActivity() != null) {
                        mLinearLoadingProcessBar.setVisibility(View.GONE);
                        String message = AlertUtils.getMessageThrowable(mContext, t);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            //Is new invoice
            llMenuBottom.setVisibility(View.GONE);
        }
    }

    private void deleteOrder(){
        if (orderId > 0){
            mLinearLoadingProcessBar.setVisibility(View.VISIBLE);

            orderAPI.deleteOrder(orderId).enqueue(new Callback<CommonResponse>() {

                @Override
                public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                    if (getActivity() != null) {
                        mLinearLoadingProcessBar.setVisibility(View.GONE);
                        CommonResponse resp = response.body();

                        if (response.isSuccessful()) {
                            if (resp.msgCode > 0){
                                Toast.makeText(getActivity(), R.string.str_delete_complete, Toast.LENGTH_SHORT).show();

                                mHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (getActivity() != null) {
                                            getActivity().finish();
                                        }
                                    }
                                }, 1500);
                            } else{
                                showAlertDialog(getString(R.string.str_cannot_delete), resp.msgString);
                            }
                        } else {
                            showAlertDialog(getString(R.string.str_cannot_delete), resp.msgString);
                        }

                        if (listOrderSellDto.size() == 0 && listOrderBuyDto.size() == 0) {
                            llMenuBottom.setVisibility(View.GONE);
                        } else {
                            llMenuBottom.setVisibility(View.VISIBLE);
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonResponse> call, Throwable t) {
                    if (getActivity() != null) {
                        llMenuBottom.setVisibility(View.GONE);
                        mLinearLoadingProcessBar.setVisibility(View.GONE);

                        String message = AlertUtils.getMessageThrowable(mContext, t);

                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
